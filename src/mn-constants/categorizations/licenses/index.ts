export interface License {
  id: string;
  description: string;
  type: string;
}
export type Licenses = License[];

// standard identifiers from https://spdx.org/licenses/preview/ in order to preserve interoperability
// prettier-ignore
export const licenses: Licenses = [
  { type: 'Creative Commons',             id: 'CC0-1.0',                         description: '( CC0 v1.0 ) : Creative Commons Zero v1.0 Universal'},
  { type: 'Creative Commons',             id: 'CC-BY-1.0',                       description: '( CC-BY v1.0 ) : Creative Commons Attribution 1.0 Generic'},
  { type: 'Creative Commons',             id: 'CC-BY-2.0',                       description: '( CC-BY v2.0 ) : Creative Commons Attribution 2.0 Generic'},
  { type: 'Creative Commons',             id: 'CC-BY-2.5',                       description: '( CC-BY v2.5 ) : Creative Commons Attribution 2.5 Generic'},
  { type: 'Creative Commons',             id: 'CC-BY-3.0',                       description: '( CC-BY v3.0 ) : Creative Commons Attribution 3.0 Unported'},
  { type: 'Creative Commons',             id: 'CC-BY-4.0',                       description: '( CC-BY v4.0 ) : Creative Commons Attribution 4.0 International'},
  { type: 'Creative Commons',             id: 'CC-BY-NC-1.0',                    description: '( CC-BY-NC v1.0 ) : Creative Commons Attribution Non Commercial 1.0 Generic'},
  { type: 'Creative Commons',             id: 'CC-BY-NC-2.0',                    description: '( CC-BY-NC v2.0 ) : Creative Commons Attribution Non Commercial 2.0 Generic'},
  { type: 'Creative Commons',             id: 'CC-BY-NC-2.5',                    description: '( CC-BY-NC v2.5 ) : Creative Commons Attribution Non Commercial 2.5 Generic'},
  { type: 'Creative Commons',             id: 'CC-BY-NC-3.0',                    description: '( CC-BY-NC v3.0 ) : Creative Commons Attribution Non Commercial 3.0 Unported'},
  { type: 'Creative Commons',             id: 'CC-BY-NC-4.0',                    description: '( CC-BY-NC v4.0 ) : Creative Commons Attribution Non Commercial 4.0 International'},
  { type: 'Creative Commons',             id: 'CC-BY-NC-ND-1.0',                 description: '( CC-BY-NC-ND v1.0 ) : Creative Commons Attribution Non Commercial No Derivatives 1.0 Generic'},
  { type: 'Creative Commons',             id: 'CC-BY-NC-ND-2.0',                 description: '( CC-BY-NC-ND v2.0 ) : Creative Commons Attribution Non Commercial No Derivatives 2.0 Generic'},
  { type: 'Creative Commons',             id: 'CC-BY-NC-ND-2.5',                 description: '( CC-BY-NC-ND v2.5 ) : Creative Commons Attribution Non Commercial No Derivatives 2.5 Generic'},
  { type: 'Creative Commons',             id: 'CC-BY-NC-ND-3.0',                 description: '( CC-BY-NC-ND v3.0 ) : Creative Commons Attribution Non Commercial No Derivatives 3.0 Unported'},
  { type: 'Creative Commons',             id: 'CC-BY-NC-ND-4.0',                 description: '( CC-BY-NC-ND v4.0 ) : Creative Commons Attribution Non Commercial No Derivatives 4.0 International'},
  { type: 'Creative Commons',             id: 'CC-BY-NC-SA-1.0',                 description: '( CC-BY-NC-SA v1.0 ) : Creative Commons Attribution Non Commercial Share Alike 1.0 Generic'},
  { type: 'Creative Commons',             id: 'CC-BY-NC-SA-2.0',                 description: '( CC-BY-NC-SA v2.0 ) : Creative Commons Attribution Non Commercial Share Alike 2.0 Generic'},
  { type: 'Creative Commons',             id: 'CC-BY-NC-SA-2.5',                 description: '( CC-BY-NC-SA v2.5 ) : Creative Commons Attribution Non Commercial Share Alike 2.5 Generic'},
  { type: 'Creative Commons',             id: 'CC-BY-NC-SA-3.0',                 description: '( CC-BY-NC-SA v3.0 ) : Creative Commons Attribution Non Commercial Share Alike 3.0 Unported'},
  { type: 'Creative Commons',             id: 'CC-BY-NC-SA-4.0',                 description: '( CC-BY-NC-SA v4.0 ) : Creative Commons Attribution Non Commercial Share Alike 4.0 International'},
  { type: 'Creative Commons',             id: 'CC-BY-ND-1.0',                    description: '( CC-BY-ND v1.0 ) : Creative Commons Attribution No Derivatives 1.0 Generic'},
  { type: 'Creative Commons',             id: 'CC-BY-ND-2.0',                    description: '( CC-BY-ND v2.0 ) : Creative Commons Attribution No Derivatives 2.0 Generic'},
  { type: 'Creative Commons',             id: 'CC-BY-ND-2.5',                    description: '( CC-BY-ND v2.5 ) : Creative Commons Attribution No Derivatives 2.5 Generic'},
  { type: 'Creative Commons',             id: 'CC-BY-ND-3.0',                    description: '( CC-BY-ND v3.0 ) : Creative Commons Attribution No Derivatives 3.0 Unported'},
  { type: 'Creative Commons',             id: 'CC-BY-ND-4.0',                    description: '( CC-BY-ND v4.0 ) : Creative Commons Attribution No Derivatives 4.0 International'},
  { type: 'Creative Commons',             id: 'CC-BY-SA-1.0',                    description: '( CC-BY-SA v1.0 ) : Creative Commons Attribution Share Alike 1.0 Generic'},
  { type: 'Creative Commons',             id: 'CC-BY-SA-2.0',                    description: '( CC-BY-SA v2.0 ) : Creative Commons Attribution Share Alike 2.0 Generic'},
  { type: 'Creative Commons',             id: 'CC-BY-SA-2.5',                    description: '( CC-BY-SA v2.5 ) : Creative Commons Attribution Share Alike 2.5 Generic'},
  { type: 'Creative Commons',             id: 'CC-BY-SA-3.0',                    description: '( CC-BY-SA v3.0 ) : Creative Commons Attribution Share Alike 3.0 Unported'},
  { type: 'Creative Commons',             id: 'CC-BY-SA-4.0',                    description: '( CC-BY-SA v4.0 ) : Creative Commons Attribution Share Alike 4.0 International'},
  { type: 'Creative Commons',             id: 'CC-P',                            description: '( CC-P ) : Creative Commons Public Domain Dedication and Certification'},
  { type: 'Others',                       id: 'GPL-1.0-or-later',                description: '( GPL 1.0 or later ) : General Public License' },
  { type: 'Others',                       id: 'LGPL-2.0-or-later',               description: '( LGPL 2.0 or later ) : Lesser General Public License' },
  { type: 'Others',                       id: 'AGPL-1.0-or-later',               description: '( AGPL 1.0 or later ) : Affero General Public License' },
  { type: 'Others',                       id: 'GFDL-1.1-or-later',               description: '( GFDL 1.1 or later ) : Free Documentation License' },
  { type: 'Others',                       id: 'MIT',                             description: '( MIT ) : Massachusetts Institute of Technology License' },
  { type: 'Others',                       id: 'LAL-1.3',                         description: '( FAL/LAL 1.3 ) : Free Art License/Licence Art Libre' },
  { type: 'Others',                       id: 'ODC-By-1.0',                      description: '( ODC ) : Open Data Commons' },
  { type: 'Others',                       id: 'OTHER-OPEN',                      description: 'Other open licence' }
];
export default licenses;
