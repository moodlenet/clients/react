import { Formats, formats, Format } from './';

export const getFormatDesc = (formats: Formats, id?: string | null) =>
  getFormat(formats, id)?.description || 'unknown';

export const getFormat = (formats: Formats, id?: string | null): Format | undefined =>
  formats.find(_ => _.id === id);

export const getFormatOption = (formats: Formats, id?: string | null) => {
  const format = getFormat(formats, id);
  return (
    format && {
      label: format.description,
      value: format.id
    }
  );
};

export const defaultFormat = getFormatOption(formats, 'CC0')!;
