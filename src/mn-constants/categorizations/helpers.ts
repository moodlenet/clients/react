import languages from './languages.json';
import types from './types.json';
import subjects from './subjects.json';
import grades from './grades.json';
import formats from './format';
import { ResourcePreviewFragment } from 'ctrl/modules/previews/resource/ResourcePreview.generated';
import { getFormatDesc } from './format/helpers';

export const getCatDesc = (list: { id: string; description: string }[], id?: string | null) =>
  getCat(list, id)?.description || 'unknown';

const getLangName = (list: { isoName: string; code: string }[], code?: string | null) =>
  getLang(list, code)?.isoName || 'unknown';

export const getResourceCatDescs = (res: ResourcePreviewFragment) => ({
  language: getLangName(languages, res.language),
  type: getCatDesc(types, res.type),
  subject: getCatDesc(subjects, res.subject),
  level: getCatDesc(grades, res.level),
  format: getFormatDesc(formats, res.format)
});

export const getCat = (list: { id: string; description: string }[], id?: string | null) =>
  list.find(_ => _.id === id);

export const getLang = (list: { isoName: string; code: string }[], code?: string | null) =>
  list.find(_ => _.code === code);

export const getCatOption = (list: { id: string; description: string }[], id?: string | null) => {
  const cat = getCat(list, id);
  return (
    cat && {
      label: cat.description,
      value: cat.id
    }
  );
};

export const getLangOption = (list: { isoName: string; code: string }[], code?: string | null) => {
  const lang = getLang(list, code);
  return (
    lang && {
      label: lang.isoName,
      value: lang.code
    }
  );
};

export const defaultGrade = getCatOption(grades, '1100')!;
export const defaultLanguage = getLangOption(languages, 'N/A')!;
export const defaultSubject = getCatOption(subjects, '2300')!;
export const defaultType = getCatOption(types, 'unk')!;
