import { i18nMark } from '@lingui/react';

import _instance_bg_img from 'static/img/moodlenet-front-image.jpg';
import _logo_large_url from 'static/img/moodlenet-logo.png';
import _logo_small_url from 'static/img/logo.jpg';
import _subjects from './categorizations/subjects.json';
import _grades from './categorizations/grades.json';
import _languages from './categorizations/languages.json';
import _types from './categorizations/types.json';
import _formats from './categorizations/format';

export { licenses } from './categorizations/licenses';

export const formats = _formats;
export const languages = _languages;
export const types = _types;
export const subjects = _subjects;
export const grades = _grades;
export const instance_bg_img = _instance_bg_img;
export const logo_large_url = _logo_large_url;
export const logo_small_url = _logo_small_url;

export const PHOENIX_SOCKET_ENDPOINT = process.env.REACT_APP_PHOENIX_SOCKET_ENDPOINT;
export const GRAPHQL_ENDPOINT = process.env.REACT_APP_GRAPHQL_ENDPOINT;
export const NODE_ENV = process.env.NODE_ENV;
export const PUBLIC_URL = process.env.PUBLIC_URL;
export const SENTRY_KEY = process.env.REACT_APP_SENTRY_API_KEY;
export const DEFAULT_PAGE_SIZE = parseInt(`${process.env.REACT_APP_DEFAULT_PAGE_SIZE}`) || 15;

export const LICENSE_INFO_LINK = 'https://blog.moodle.net/2019/free-cultural-works/';

export const APP_NAME = 'MoodleNet';
export const INSTANCE_DESCRIPTION =
  process.env.INSTANCE_DESCRIPTION ||
  'Welcome to Moodle’s primary MoodleNet site, part of a federated open-source social network where educators can find each other and build collections of the best open educational resources (OER) in the world.';
export const INVITE_ONLY_TEXT =
  process.env
    .REACT_APP_INVITE_ONLY_TEXT; /* ||
  'Please note, signups on this instance are currently invite-only.'; */
export const INSTANCE_TAGLINE = i18nMark(
  'Share, Curate and Discuss the best Open Educational Resources.'
);
export const INSTANCE_PROMPT = i18nMark(
  "You don't need to sign up to preview what people are sharing and discussing publicly."
);
export const prompt_signin = i18nMark('Sign in');
export const my_timeline = i18nMark('My MoodleNet');

export const terms_markdown_urls = {
  // replace the URLs as needed, or enable/disable to use `terms_markdown_text` instead
  enabled: true,
  terms_users: '/static/terms.md'
};

export const related_urls = {
  // replace the URLs as needed
  project_homepage: 'https://moodle.net',
  code: 'https://gitlab.com/moodlenet',
  feedback: 'https://changemap.co/moodle/moodlenet/'
};

export const IS_DEV = NODE_ENV === 'development';

export type LocaleDef = { code: string; desc: string; rtl: boolean };

export const locales: LocaleDef[] = [
  { code: 'en_GB', desc: 'English, British', rtl: false },
  { code: 'en_US', desc: 'English, USA', rtl: false },
  { code: 'es_MX', desc: 'Español, Méjico', rtl: false },
  { code: 'es_ES', desc: 'Español, España', rtl: false },
  { code: 'fr_FR', desc: 'Français, France', rtl: false },
  { code: 'eu', desc: 'Euskara', rtl: false },
  { code: 'bn_IN', desc: 'বাংলা', rtl: false },
  { code: 'ca', desc: 'català, valencià', rtl: false },
  { code: 'de', desc: 'Deutsch', rtl: false },
  { code: 'el_GR', desc: 'ελληνικά', rtl: false },
  { code: 'he', desc: 'עברית', rtl: false },
  { code: 'hi_IN', desc: 'हिन्दी, हिंदी', rtl: false },
  { code: 'pt_BR', desc: 'Portuguese, Brazil', rtl: false },
  { code: 'ru', desc: 'русский', rtl: false },
  { code: 'uk_UA', desc: 'Українська', rtl: false },
  { code: 'ur', desc: 'اردو', rtl: false },
  { code: 'zh_CN', desc: '中文 (Zhōngwén), 汉语, 漢語', rtl: false },
  { code: 'ar', desc: 'العربية, المملكة العربية السعودية', rtl: true }
];

const mothershipAppId = process.env.REACT_APP_MOTHERSHIP_API_ID;
const mothershipApiKey = process.env.REACT_APP_MOTHERSHIP_API_KEY;
const mothershipEnv = process.env.REACT_APP_MOTHERSHIP_ENV;

export const mothershipCreds =
  mothershipAppId && mothershipApiKey
    ? {
        appId: mothershipAppId,
        apiKey: mothershipApiKey,
        indexName: mothershipEnv
      }
    : null;
export const searchDisabled = !mothershipCreds;

export const USERNAME_REGEX = /^[a-zA-Z][a-zA-Z0-9-]{2,}$/;
export const EMAIL_REGEX = /^.+@.+\..+$/;
export const DOMAIN_REGEX = /^.+\..+$/;

export const nord = {
  // Main
  app: '#2E3440',
  appInverse: '#3B4252',
  primary: '#BF616A',
  secondary: '#D08770',
  tertiary: '#EBCB8B',

  // Status
  positive: '#2db783',
  negative: '#ff5a5f',
  warning: '#ffebb3',

  // Monochrome
  lightest: '#333333',
  lighter: '#444444',
  light: '#666666',
  mediumlight: '#999999',
  medium: '#DDDDDD',
  mediumdark: '#EEEEEE',
  dark: '#F3F3F3',
  darker: '#F8F8F8',
  darkest: '#FFFFFF',
  border: '1px solid #2E3440'
};

export const colors = {
  // Main
  app: 'rgb(245, 246, 247)',
  appInverse: '#fff',
  primary: '#f98012',
  secondary: '#1EA7FD',
  tertiary: '#DDDDDD',

  // Status
  positive: '#2db783',
  negative: '#ff5a5f',
  warning: '#ffebb3',

  // Monochrome
  lightest: '#FFFFFF',
  lighter: '#F8F8F8',
  light: '#F3F3F3',
  mediumlight: '#EEEEEE',
  medium: '#DDDDDD',
  mediumdark: '#999999',
  dark: '#666666',
  darker: '#444444',
  darkest: '#333333',
  border: '1px solid #F3F3F3'
};

export const typography = {
  type: {
    primary: '"Open Sans", sans-serif'
  },
  size: {
    s1: '12px',
    s2: '14px',
    s3: '16px',
    m1: '20px',
    m2: '24px',
    m3: '28px',
    l1: '32px',
    l2: '40px',
    l3: '48px',
    code: '90px'
  }
};

/* log ENV if DEV */
IS_DEV &&
  console.log(`-environment-
${Object.entries(process.env)
  .map(([key, value]) => `${key}=${value}`)
  .join('\n')}
-------------
`);
/***/
