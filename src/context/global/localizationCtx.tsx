import { Settings } from 'luxon';
import { Catalog, Catalogs, I18n, setupI18n } from '@lingui/core';
import { I18nProvider } from '@lingui/react';
import React, { createContext, useEffect, useMemo, useState, useCallback } from 'react';
import { IS_DEV, LocaleDef, locales as available } from '../../mn-constants';
import { createLocalSessionKVStorage, LOCAL } from 'util/keyvaluestore/localSessionStorage';
import { toast } from 'react-toastify';

const LOCALIZATION_STORE = 'LOCALIZATION';
const LOCALE_KEY = '#locale';
const kvstore = createLocalSessionKVStorage(LOCAL)(LOCALIZATION_STORE);
export type LocaleContextT = {
  current: LocaleDef;
  i18n: I18n;
  available: LocaleDef[];
  set(locale: LocaleDef): void;
};
const getStoredLocaleCode = (): string | null => kvstore.get(LOCALE_KEY);
const setStoredLocaleCode = (localeCode: string): void => kvstore.set(LOCALE_KEY, localeCode);

const savedLangCode = getStoredLocaleCode();
let defaultLocale =
  (savedLangCode && available.find(locale => locale.code === savedLangCode)) || available[0];

export const i18n = setupI18n({
  locales: available.map(locale => locale.code)
});
export const LocaleContext = createContext<LocaleContextT>({
  current: defaultLocale,
  i18n,
  available,
  set: () => void 0
});
export const ProvideLocalizationCtx: React.FC = ({ children }) => {
  const [current, setCurrent] = useState(defaultLocale);
  const [catalogs, setCatalogs] = useState<Catalogs>({});

  const set = useCallback(
    (locale: LocaleDef) => {
      setCurrent(locale);
      const { code } = locale;
      Settings.defaultLocale = code.split('_')[0];
      setStoredLocaleCode(code);
    },
    [setCurrent]
  );

  useEffect(() => {
    setHTMLDirection(current.rtl);
    loadCatalog(current.code)
      .then(cat => {
        if (!cat.languageData) {
          return Promise.reject(`empty catalog for ${current.code}`);
        }
        if (!catalogs[current.code]) {
          setCatalogs({ ...catalogs, [current.code]: cat });
        }
        defaultLocale = current;
      })
      .catch(err => {
        console.error(`Error loading Locale: ${current.code}`, err);
        toast(`We encountered some issue applying ${current.desc}`, { type: 'warning' });
        set(defaultLocale);
      });
  }, [catalogs, current, set]);

  const localeContextValue = useMemo<LocaleContextT>(
    () => ({
      current,
      available,
      i18n,
      set
    }),
    [current, set]
  );

  return (
    <I18nProvider i18n={i18n} language={current.code} catalogs={catalogs}>
      <LocaleContext.Provider value={localeContextValue}>{children}</LocaleContext.Provider>
    </I18nProvider>
  );
};

const loadCatalog = async (localeCode: string): Promise<Catalog> => {
  if (IS_DEV) {
    return import(
      /* webpackMode: "lazy", webpackChunkName: "i18n-[index]" */
      `@lingui/loader!../../locales/${localeCode}/messages.po`
    );
  } else {
    return import(
      /* webpackMode: "lazy", webpackChunkName: "i18n-[index]" */
      // `../../locales/${localeCode}/messages.js`
      // FIXME: loading po files instead of compiled js, because of
      // https://tracker.moodle.org/browse/MDLNET-475?focusedCommentId=810255&page=com.atlassian.jira.plugin.system.issuetabpanels%3Acomment-tabpanel#comment-810255
      `@lingui/loader!../../locales/${localeCode}/messages.po`
    );
  }
};

const setHTMLDirection = (RTL: boolean) => {
  const htmlEl = document.querySelector('html');
  if (htmlEl) {
    const dir = RTL ? 'rtl' : 'ltr';
    htmlEl.style.direction = dir;
    htmlEl.classList.remove('--rtl', '--ltr');
    htmlEl.classList.add(`--${dir}`);
  }
};
