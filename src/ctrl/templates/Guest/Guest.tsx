import { MainHeaderCtrl } from 'ctrl/modules/Header/Header';
import React, { FC } from 'react';
import { Guest, Props } from 'ui/templates/guest';
import { ProvideSideBarContext } from 'ctrl/context/SideBar';
import { SearchBox } from 'ctrl/modules/SearchBox/SearchBox';
import { loginLocation } from 'routes/LoginPageRoute';

export interface GuestTemplate {
  withoutHeader?: boolean;
}
export const GuestTemplate: FC<GuestTemplate> = ({ children, withoutHeader = false }) => {
  const HeaderBox = withoutHeader ? undefined : <MainHeaderCtrl />;
  const props: Props = {
    HeaderBox,
    SearchBox: <SearchBox />,
    loginLink: loginLocation.getPath(undefined, undefined)
  };
  return (
    <ProvideSideBarContext>
      <Guest {...props}>{children}</Guest>
    </ProvideSideBarContext>
  );
};
