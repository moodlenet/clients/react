import { useMe } from 'fe/session/useMe';
import { MainHeaderCtrl } from 'ctrl/modules/Header/Header';
import React, { FC, useMemo } from 'react';
import { Props, WithoutSidebar } from 'ui/templates/withoutSidebar';
import { GuestTemplate } from '../Guest/Guest';
import { SidebarCtrl } from 'ctrl/modules/Sidebar/Sidebar';
import { SearchBox } from 'ctrl/modules/SearchBox/SearchBox';
import { homeLocation } from 'routes/HomePageRoute';
import { userLocation } from 'routes/UserPageRoute';

export interface WithoutSidebarTemplate {}
export const WithoutSidebarTemplate: FC<WithoutSidebarTemplate> = ({ children }) => {
  const { me, logout } = useMe();
  const withoutSidebarProps = useMemo<null | Props>(() => {
    const user = me?.user;
    if (!user) {
      return null;
    }
    const userImage = user.icon?.url || '';
    const userLink = userLocation.getPath({ userId: user.id, tab: undefined }, undefined);
    const props: Props = {
      SidebarBox: <SidebarCtrl />,
      HeaderBox: <MainHeaderCtrl />,
      SearchBox: <SearchBox />,
      userImage,
      userLink,
      homeLink: homeLocation.getPath(undefined, undefined),
      signout: logout,
      username: user.displayUsername || '',
      name: user.preferredUsername || ''
    };
    return props;
  }, [logout, me]);

  return withoutSidebarProps ? (
    <WithoutSidebar {...withoutSidebarProps}>{children}</WithoutSidebar>
  ) : (
    <GuestTemplate>{children}</GuestTemplate>
  );
};
