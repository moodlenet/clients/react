import { t } from '@lingui/macro';
import { usePageTitle } from 'context/global/pageCtx';
import { useCollection } from 'fe/collection/useCollection';
import { useCollectionResources } from 'fe/resource/collection/useCollectionResources';
import { useCollectionFollowers } from 'fe/user/followers/collection/useCollectionFollowers';
import { Collection } from 'graphql/types.generated';
import { useNotifyMustLogin } from 'ctrl/lib/notifyMustLogin';
import { AddResourceCtrl, AddResourceResponse } from 'ctrl/modules/AddResource/addResourceCtrl';
import { HeroCollectionCtrl } from 'ctrl/modules/HeroCollection/HeroCollection';
import { ResourcePreviewCtrl } from 'ctrl/modules/previews/resource/ResourcePreview';
import { UserPreviewCtrl } from 'ctrl/modules/previews/user/UserPreview';
import { ShareLinkCtrl, ShareLinkResponse } from 'ctrl/modules/ShareLink/shareLinkCtrl';
import React, { FC, ReactElement, useCallback, useMemo, useState } from 'react';
import CollectionPageUI, { Props as CollectionPageProps } from 'ui/pages/collection';
import { collectionLocation } from 'routes/CollectionPageRoute';
import { communityLocation } from 'routes/CommunityPageRoute';
import { useAddResource } from 'fe/resource/add/useAddResource';

export enum CollectionPageTab {
  // Activities,
  Resources,
  Followers
}
export interface CollectionPage {
  collectionId: Collection['id'];
  tab: CollectionPageTab;
  basePath: string;
}
const collectionPageFollowersTitle = t`Collection {name} - Followers`;
const collectionPageResourcesTitle = t`Collection {name} - Resources`;

type OpenPanel = 'share' | 'upload';

export const CollectionPage: FC<CollectionPage> = props => {
  const { create: createResource } = useAddResource();

  const [openPanel, setOpenPanel] = useState<OpenPanel>();
  const closePanel = useCallback(() => setOpenPanel(undefined), []);
  const { collectionId, basePath, tab } = props;
  const { collection, isCommunityMember } = useCollection(props.collectionId);
  const collectionPageTitle =
    tab === CollectionPageTab.Followers
      ? collectionPageFollowersTitle
      : CollectionPageTab.Resources
      ? collectionPageResourcesTitle
      : collectionPageResourcesTitle; //never;

  usePageTitle(!!collection?.name && collectionPageTitle, collection);

  const { collectionFollowersPage } = useCollectionFollowers(props.collectionId);
  const [loadMoreFollowers] = collectionFollowersPage.formiks;

  const { resourcesPage } = useCollectionResources(props.collectionId);
  const [loadMoreResources] = resourcesPage.formiks;

  const notifyNotLogged = useNotifyMustLogin();

  const HeroCollection = <HeroCollectionCtrl basePath={basePath} collectionId={collectionId} />;

  const Resources = resourcesPage.edges
    .map(resource => <ResourcePreviewCtrl resourceId={resource.id} key={resource.id} />)
    .filter((_): _ is ReactElement => !!_);

  const Followers: CollectionPageProps['Followers'] = collectionFollowersPage.edges
    .map(
      follow =>
        follow.creator && (
          <UserPreviewCtrl userId={follow.creator.userId} key={follow.creator.userId} />
        )
    )
    .filter((_): _ is ReactElement => !!_);

  const createResourceCb = useCallback(
    (resp: AddResourceResponse | ShareLinkResponse | null) =>
      Promise.resolve(
        resp &&
          createResource({
            collectionId,
            resource: resp.resource,
            content: resp.content,
            icon: resp.icon
          })
      ).then(closePanel),
    [closePanel, collectionId, createResource]
  );
  const UploadResourcePanel =
    openPanel === 'upload' ? <AddResourceCtrl done={createResourceCb} /> : null;

  const ShareLink = openPanel === 'share' ? <ShareLinkCtrl done={createResourceCb} /> : null;

  const collectionPageProps = useMemo<CollectionPageProps | null>(() => {
    if (!collection) {
      return null;
    }
    const tabPaths: CollectionPageProps['tabPaths'] = {
      followers: collectionLocation.getPath({ collectionId, tab: 'followers' }, undefined),
      resources: collectionLocation.getPath({ collectionId, tab: undefined }, undefined)
    };
    const uiProps: CollectionPageProps = {
      ShareLink,
      HeroCollection,
      Resources,
      UploadResourcePanel,
      tabPaths,
      Followers,
      communityIcon: collection.community?.icon?.url || '',
      communityLink: collection.community
        ? communityLocation.getPath(
            { communityId: collection.community.id, tab: undefined },
            undefined
          )
        : '',
      communityName: collection.community ? collection.community.name : '',
      loadMoreFollowers,
      loadMoreResources,
      isCommunityMember,
      shareLink: () =>
        setOpenPanel(openPanel === 'share' || notifyNotLogged() ? undefined : 'share'),
      upload: () => setOpenPanel(openPanel === 'upload' || notifyNotLogged() ? undefined : 'upload')
    };
    return uiProps;
  }, [
    collection,
    collectionId,
    ShareLink,
    HeroCollection,
    Resources,
    UploadResourcePanel,
    Followers,
    loadMoreFollowers,
    loadMoreResources,
    isCommunityMember,
    openPanel,
    notifyNotLogged
  ]);
  return collectionPageProps && <CollectionPageUI {...collectionPageProps} />;
};
