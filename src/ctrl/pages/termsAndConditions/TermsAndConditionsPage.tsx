import { t } from '@lingui/macro';
import useAxios from 'axios-hooks';
import { usePageTitle } from 'context/global/pageCtx';
import { terms_markdown_urls } from 'mn-constants';
import React, { FC, useEffect, useMemo } from 'react';
import TermsAndConditionsPage, { Props } from 'ui/pages/termsAndConditions';
const termsAndConditionsPageTitle = t`Terms and Conditions`;

export const TermsAndConditionsPageCtrl: FC = () => {
  usePageTitle(termsAndConditionsPageTitle);

  const [terms_users, exec_terms_users] = useAxios<string | undefined>(
    terms_markdown_urls.terms_users,
    {
      useCache: true,
      manual: true
    }
  );

  useEffect(
    () => {
      if (terms_markdown_urls.enabled) {
        exec_terms_users();
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  );

  const props = useMemo<Props | null>(
    () =>
      !terms_users.data
        ? null
        : {
            terms_users_data: terms_users.data
          },
    [terms_users.data]
  );

  return props && <TermsAndConditionsPage {...props} />;
};
