import { useUserOutboxActivities } from 'fe/activities/outbox/user/useUserOutboxActivities';
import { useUserFollowedCollections } from 'fe/collection/user/useUserFollowedCollections';
import { UserFollowedCollectionFragment } from 'fe/collection/user/useUserFollowedCollections.generated';
import { useUserFollowedCommunities } from 'fe/community/user/useUserFollowedCommunities';
import { UserFollowedCommunityFragment } from 'fe/community/user/useUserFollowedCommunities.generated';
import { getActivityActor } from 'fe/lib/activity/getActivityActor';
import { getEventStringByContext } from 'fe/lib/activity/getActivityEventString';
import { getCommunityInfoStrings } from 'fe/lib/activity/getContextCommunityInfo';
import { useUserLikes } from 'fe/likes/user/useUserLikes';
import { useUserFollowedUsers } from 'fe/user/followed/user/useUserFollowedUsers';
import { UserFollowedUserFragment } from 'fe/user/followed/user/useUserFollowedUsers.generated';
import { useUser } from 'fe/user/useUser';
import { ActivityVerb, User } from 'graphql/types.generated';
import { HeroUser } from 'ctrl/modules/HeroUser/HeroUser';
import { ActivityPreviewCtrl } from 'ctrl/modules/previews/activity/ActivityPreview';
import { PreviewComponent } from 'ctrl/modules/previews/activity/PreviewComponent';
import { CollectionPreviewCtrl } from 'ctrl/modules/previews/collection/CollectionPreview';
import { LikedCommentPreviewCtrl } from 'ctrl/modules/previews/commentLiked/CommentLikedPreview';
import { CommunityPreviewCtrl } from 'ctrl/modules/previews/community/CommunityPreview';
import { UserPreviewCtrl } from 'ctrl/modules/previews/user/UserPreview';
import React, { FC, useMemo } from 'react';
import { Box } from 'rebass';
import { ActivityPreview, Status, Props as ActivityPreviewProps } from 'ui/modules/ActivityPreview';
import { Props, User as UserPageUI } from 'ui/pages/user';
import { t } from '@lingui/macro';
import { usePageTitle } from 'context/global/pageCtx';
import { userLocation } from 'routes/UserPageRoute';
export interface UserPage {
  userId: User['id'];
  tab: UserPageTab;
  basePath: string;
}
export enum UserPageTab {
  Starred,
  Communities,
  Collections,
  Following,
  Activities
}

const userStarredPageTitle = t`User {name} - Starred`;
const userCommunitiesPageTitle = t`User {name} - Communities`;
const userCollectionsPageTitle = t`User {name} - Collections`;
const userFollowingPageTitle = t`User {name} - Following`;
const userActivitiesPageTitle = t`User {name} - Activities`;

export const UserPage: FC<UserPage> = ({ userId, basePath, tab }) => {
  const userInfo = useUser(userId);

  const userPageTitle =
    tab === UserPageTab.Starred
      ? userStarredPageTitle
      : tab === UserPageTab.Communities
      ? userCommunitiesPageTitle
      : tab === UserPageTab.Collections
      ? userCollectionsPageTitle
      : tab === UserPageTab.Following
      ? userFollowingPageTitle
      : tab === UserPageTab.Activities
      ? userActivitiesPageTitle
      : userActivitiesPageTitle; //never
  usePageTitle(!!userInfo?.user?.name && userPageTitle, userInfo.user);

  const { likesPage } = useUserLikes(userId);
  const [loadMoreLikes] = likesPage.formiks;

  const { activitiesPage } = useUserOutboxActivities(userId);
  const [loadMoreActivities] = activitiesPage.formiks;

  const { followedCollectionsPage } = useUserFollowedCollections(userId);
  const [loadMoreCollections] = followedCollectionsPage.formiks;

  const { followedCommunitiesPage } = useUserFollowedCommunities(userId);
  const [loadMoreCommunities] = followedCommunitiesPage.formiks;

  const { followedUsersPage } = useUserFollowedUsers(userId);
  const [loadMoreFollowing] = followedUsersPage.formiks;

  const userPageProps = useMemo<Props>(() => {
    const { totalActivities, totalCollections, totalCommunities, totalUsers } = userInfo;
    const LikesBoxes = (
      <>
        {likesPage.edges.map(like => {
          const { communityLink, communityName } = getCommunityInfoStrings(like.context);
          const actor = userInfo.user ? getActivityActor(userInfo.user) : null;
          const activityContext = like;
          const event = getEventStringByContext(activityContext, ActivityVerb.Created);
          const preview =
            like.context.__typename === 'Comment' ? (
              <LikedCommentPreviewCtrl key={like.id} commentId={like.context.id} />
            ) : (
              <PreviewComponent context={activityContext} />
            );
          const activityProps: ActivityPreviewProps = {
            actor,
            communityLink,
            communityName,
            createdAt: like.createdAt,
            event,
            status: Status.Loaded,
            preview
          };
          // console.log(activityProps, likesPage);
          return <ActivityPreview {...activityProps} key={activityContext.id} />;
        })}
      </>
    );
    const ActivityBoxes = (
      <>
        {activitiesPage.edges.map(activity => (
          <ActivityPreviewCtrl activityId={activity.id} key={activity.id} />
        ))}
      </>
    );
    const CollectionsBoxes = (
      <>
        {followedCollectionsPage.edges
          .map(follow => follow.context)
          .filter(
            (context): context is UserFollowedCollectionFragment =>
              context.__typename === 'Collection'
          )
          .map(followedCollection => (
            <Box m={2} mb={0} key={followedCollection.id}>
              <CollectionPreviewCtrl
                collectionId={followedCollection.id}
                key={followedCollection.id}
              />
            </Box>
          ))}
      </>
    );
    const CommunityBoxes = (
      <>
        {followedCommunitiesPage.edges
          .map(follow => follow.context)
          .filter(
            (context): context is UserFollowedCommunityFragment =>
              context.__typename === 'Community'
          )
          .map(followedCommunity => (
            <CommunityPreviewCtrl communityId={followedCommunity.id} key={followedCommunity.id} />
          ))}
      </>
    );

    const UserBoxes = (
      <>
        {followedUsersPage.edges
          .map(follow => follow.context)
          .filter((context): context is UserFollowedUserFragment => context.__typename === 'User')
          .map(followedUser => (
            <UserPreviewCtrl userId={followedUser.userId} key={followedUser.userId} />
          ))}
      </>
    );

    const tabPaths: Props['tabPaths'] = {
      collections: userLocation.getPath({ userId, tab: 'collections' }, undefined),
      communities: userLocation.getPath({ userId, tab: 'communities' }, undefined),
      starred: userLocation.getPath({ userId, tab: 'starred' }, undefined),
      timeline: userLocation.getPath({ userId, tab: undefined }, undefined)
    };

    const HeroUserBox = <HeroUser userId={userId} />;

    const props: Props = {
      ActivityBoxes,
      LikesBoxes,
      HeroUserBox,
      CollectionsBoxes,
      CommunityBoxes,
      UserBoxes,
      userName: userInfo.user?.name || '',
      totalActivities: `${totalActivities || '0'}`,
      totalCollections: `${totalCollections || '0'}`,
      totalCommunities: `${totalCommunities || '0'}`,
      totalUsers: `${totalUsers || '0'}`,
      userLink: userInfo.user?.website || '',
      loadMoreActivities,
      loadMoreCollections,
      loadMoreCommunities,
      loadMoreFollowing,
      loadMoreLikes,
      tabPaths
    };
    return props;
  }, [
    userInfo,
    likesPage,
    activitiesPage.edges,
    followedCollectionsPage.edges,
    followedCommunitiesPage.edges,
    followedUsersPage.edges,
    userId,
    loadMoreActivities,
    loadMoreCollections,
    loadMoreCommunities,
    loadMoreFollowing,
    loadMoreLikes
  ]);
  return <UserPageUI {...userPageProps} />;
};
