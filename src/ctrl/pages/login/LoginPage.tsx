import { useFormik } from 'formik';
import React, { FC } from 'react';
import Login, { LoginFormValues, Props as LoginPageProps } from 'ui/pages/login';
import * as Yup from 'yup';
import { useAnon } from 'fe/session/useAnon';
import { t } from '@lingui/macro';
import { usePageTitle } from 'context/global/pageCtx';
import { discoverLocation } from 'routes/DiscoverPageRoute';
import { resetPasswordLocation } from 'routes/ResetPasswordPageRoute';
import { signupLocation } from 'routes/SignupPageRoute';

export const validationSchema: Yup.ObjectSchema<LoginFormValues> = Yup.object<LoginFormValues>({
  email: Yup.string()
    .max(50)
    .required(),
  password: Yup.string()
    .max(50)
    .required()
});
export interface Props {}

const loginPageTitle = t`Login`;

export const LoginPageCtrl: FC<Props> = () => {
  usePageTitle(loginPageTitle);
  const { login } = useAnon();
  const formik = useFormik<LoginFormValues>({
    initialValues: {
      email: '',
      password: ''
    },
    enableReinitialize: true,
    onSubmit: ({ email, password }) => login(email, password),
    validationSchema
  });
  const props: LoginPageProps = {
    formik,
    discoverPageLink: discoverLocation.getPath({ tab: undefined }, undefined),
    resetPageLink: resetPasswordLocation.getPath(undefined, undefined),
    signupPageLink: signupLocation.getPath(undefined, undefined)
  };
  return <Login {...props} />;
};
