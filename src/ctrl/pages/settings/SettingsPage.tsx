import { useMe } from 'fe/session/useMe';
import { useFormik } from 'formik';
import React, { FC, useMemo } from 'react';
import {
  EditProfileFormValues,
  Props as SettingsUIProps,
  Settings as SettingsPageUI
} from 'ui/pages/settings';
import { InstanceFlagsSection } from './flags/InstanceFlagsSection';
import { InstanceSettingsSection } from './instance/InstanceSettingsSection';
import { InstanceInvitesSection } from './invites/InstanceInvitesSection';
import { InstanceModerationLogSection } from './moderationLog/InstanceModerationLogSection';
import { PreferencesSettingsSection } from './preferences/PreferencesSettingsSection';
import { t } from '@lingui/macro';
import { usePageTitle } from 'context/global/pageCtx';
import { settingsLocation } from 'routes/SettingsPageRoute';
import { DropzoneAreaCtrl } from 'ctrl/modules/DropzoneArea/DropzoneAreaCtrl';
import * as Yup from 'yup';
import { TestUrlOrFile } from 'ctrl/lib/formik-validations';

interface EditProfileFormValuesWithIconAndImage extends EditProfileFormValues {
  image: File | string | undefined;
  icon: File | string | undefined;
}

export enum SettingsPageTab {
  Preferences,
  Invites,
  Instance,
  Flags,
  ModerationLogs,
  General
}

export interface SettingsPage {
  tab: SettingsPageTab;
}

const settingsPreferencesPageTitle = t`Settings - Preferences`;
const settingsInvitesPageTitle = t`Settings - Invites`;
const settingsInstancePageTitle = t`Settings - Instance`;
const settingsFlagsPageTitle = t`Settings - Flags`;
const settingsModerationLogsPageTitle = t`Settings - Moderation`;
const settingsGeneralPageTitle = t`Settings - General`;

export const validationSchema = Yup.object<EditProfileFormValuesWithIconAndImage>({
  name: Yup.string()
    .max(90)
    .required(),
  summary: Yup.string().max(1000),
  image: Yup.mixed<File | string>().test(...TestUrlOrFile),
  location: Yup.string().max(1000),
  website: Yup.string().max(1000),
  icon: Yup.mixed<File | string>().test(...TestUrlOrFile),
  email: Yup.string()
    .max(90)
    .required()
});

export const SettingsPage: FC<SettingsPage> = ({ tab }) => {
  const settingsPageTitle =
    tab === SettingsPageTab.Preferences
      ? settingsPreferencesPageTitle
      : tab === SettingsPageTab.Invites
      ? settingsInvitesPageTitle
      : tab === SettingsPageTab.Instance
      ? settingsInstancePageTitle
      : tab === SettingsPageTab.Flags
      ? settingsFlagsPageTitle
      : tab === SettingsPageTab.ModerationLogs
      ? settingsModerationLogsPageTitle
      : tab === SettingsPageTab.General
      ? settingsGeneralPageTitle
      : settingsGeneralPageTitle; //never
  usePageTitle(settingsPageTitle);

  const { me, updateProfile } = useMe();
  const profile = me?.user;

  const initialValues = useMemo<EditProfileFormValuesWithIconAndImage>(
    () => ({
      email: me?.email || '',
      icon: profile?.icon?.url || undefined,
      image: profile?.image?.url || undefined,
      location: profile?.location || '',
      name: profile?.name || '',
      website: profile?.website || '',
      summary: profile?.summary || ''
    }),
    [me, profile]
  );

  const updateProfileFormik = useFormik<EditProfileFormValuesWithIconAndImage>({
    initialValues,
    validationSchema,
    enableReinitialize: true,
    onSubmit: ({ icon, image, ...profile }) => updateProfile({ profile, icon, image })
  });
  const sectionPaths: SettingsUIProps['sectionPaths'] = useMemo(
    () => ({
      preferences: settingsLocation.getPath({ tab: 'preferences' }, undefined),
      instance: settingsLocation.getPath({ tab: 'instance' }, undefined),
      invites: settingsLocation.getPath({ tab: 'invites' }, undefined),
      flags: settingsLocation.getPath({ tab: 'flags' }, undefined),
      logs: settingsLocation.getPath({ tab: 'logs' }, undefined),
      general: settingsLocation.getPath({ tab: undefined }, undefined)
    }),
    []
  );
  const settingsPageProps = useMemo<SettingsUIProps | null>(() => {
    const ProfileIconElement = (
      <DropzoneAreaCtrl
        imageOnly
        imageUrl={profile?.icon?.url}
        onFileSelected={icon =>
          updateProfileFormik.setValues({ ...updateProfileFormik.values, icon }, false)
        }
      />
    );
    const ProfileImageElement = (
      <DropzoneAreaCtrl
        imageOnly
        imageUrl={profile?.image?.url}
        onFileSelected={image =>
          updateProfileFormik.setValues({ ...updateProfileFormik.values, image }, false)
        }
      />
    );
    const props: SettingsUIProps = {
      ProfileIconElement,
      ProfileImageElement,
      sectionPaths,
      displayUsername: profile?.displayUsername || '',
      isAdmin: !!me?.isInstanceAdmin,
      formik: updateProfileFormik,
      Preferences: <PreferencesSettingsSection />,
      Instance: <InstanceSettingsSection />,
      Invites: <InstanceInvitesSection />,
      Flags: <InstanceFlagsSection />,
      ModerationLog: <InstanceModerationLogSection />
    };
    return props;
  }, [me, profile, sectionPaths, updateProfileFormik]);

  return settingsPageProps && <SettingsPageUI {...settingsPageProps} />;
};
