import React, { FC, useState, useEffect, useContext } from 'react';
import { DropzoneArea } from 'ui/modules/DropzoneArea';
import { useDropzone } from 'react-dropzone';
import { useInstanceInfoQuery } from 'fe/instance/info/useInstanceInfo.generated';
import { LocaleContext } from 'context/global/localizationCtx';
import { t } from '@lingui/macro';

export interface DropzoneAreaCtrl {
  fileName?: undefined | string;
  imageOnly?: boolean;
  imageUrl?: undefined | string;
  onFileSelected(_: File | undefined): unknown;
  error?: undefined | string;
}
const tooBigMsgI18n = t`File {filename} is too large. You may not upload files larger than {MB} MB`;
const rejectedFileMsgI18n = t`File {filename} can't be selected`;
const ANY_FILE_PATTERN = undefined;
const IMAGE_ONLY_PATTERN = 'image/*';
const DEFAULT_MAX_UPLOAD_BYTES = 10 * 1024 * 1024;

export const DropzoneAreaCtrl: FC<DropzoneAreaCtrl> = ({
  fileName,
  imageOnly,
  imageUrl,
  onFileSelected,
  error
}) => {
  const [rejectedFileErrorMsg, setRejectedFileErrorMsg] = useState<string>();
  const { i18n } = useContext(LocaleContext);
  const { data: instanceInfoQ } = useInstanceInfoQuery();
  const uploadMaxBytes = instanceInfoQ?.instance?.uploadMaxBytes || DEFAULT_MAX_UPLOAD_BYTES;
  const [currentImageUrl, setCurrentImageUrl] = useState<string | undefined>(imageUrl);
  useEffect(
    () => () => {
      currentImageUrl && URL.revokeObjectURL(currentImageUrl);
    },
    [currentImageUrl]
  );
  const { getRootProps, getInputProps, isDragActive, acceptedFiles } = useDropzone({
    accept: imageOnly ? IMAGE_ONLY_PATTERN : ANY_FILE_PATTERN,
    maxSize: uploadMaxBytes,
    onDrop: (acceptedDropFiles, rejectedFiles) => {
      const rejectedFile = rejectedFiles[0];
      if (rejectedFile) {
        if (rejectedFile.size > uploadMaxBytes) {
          setRejectedFileErrorMsg(
            i18n._({
              ...tooBigMsgI18n,
              values: { filename: rejectedFile.name, MB: toMegabytes(uploadMaxBytes) }
            })
          );
        } else {
          setRejectedFileErrorMsg(
            i18n._({ ...rejectedFileMsgI18n, values: { filename: rejectedFile.name } })
          );
        }
      } else {
        setRejectedFileErrorMsg(undefined);
      }
      const file: File | undefined = acceptedDropFiles[0];
      setCurrentImageUrl(isImageFile(file) ? URL.createObjectURL(file) : undefined);
      onFileSelected(file);
    }
  });
  const currentFile: File | undefined = acceptedFiles[0];
  const isImage = imageOnly || isImageFile(currentFile);
  const currentFilename = currentFile?.name || fileName;
  // console.table({
  //   isImage,
  //   currentFilename,
  //   isDragActive,
  //   currentImageUrl,
  //   '****': null,
  //   fileName,
  //   imageOnly,
  //   imageUrl
  // });
  const errMsg = [error || '', rejectedFileErrorMsg || ''].join('\n').trim();
  return (
    <>
      <input {...getInputProps()} />
      <DropzoneArea
        error={errMsg}
        isImage={isImage}
        isActive={isDragActive}
        currentFilename={currentFilename}
        currentImageUrl={currentImageUrl}
        mainDivProps={getRootProps()}
      />
    </>
  );
};

export const isImageFile = (file: File | undefined) =>
  file ? file.type.indexOf('image') > -1 : false;

export const toMegabytes = (bytes: number) => (bytes / 1024 / 1024).toFixed(2);
