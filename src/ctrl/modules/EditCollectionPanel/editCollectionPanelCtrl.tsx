import { useEditCollection } from 'fe/collection/edit/useEditCollection';
import { useFormik } from 'formik';
import { Collection } from 'graphql/types.generated';
import React, { FC, useMemo } from 'react';
import { EditCollectionFormValues, EditCollectionPanel } from 'ui/modules/EditCollectionPanel';
import * as Yup from 'yup';
import { TestUrlOrFile } from 'ctrl/lib/formik-validations';
import { DropzoneAreaCtrl } from '../DropzoneArea/DropzoneAreaCtrl';

export interface EditCollectionFormValuesWithIcon extends EditCollectionFormValues {
  icon: string | File | undefined;
}
export const validationSchema: Yup.ObjectSchema<EditCollectionFormValuesWithIcon> = Yup.object<
  EditCollectionFormValuesWithIcon
>({
  name: Yup.string()
    .min(2)
    .max(60)
    .required(),
  summary: Yup.string().max(500),
  icon: Yup.mixed<File | string>().test(...TestUrlOrFile)
});

export interface Props {
  collectionId: Collection['id'];
  done(): any;
}
export const EditCollectionPanelCtrl: FC<Props> = ({ done, collectionId }: Props) => {
  const { collection, edit } = useEditCollection(collectionId);
  const initialValues = useMemo<EditCollectionFormValuesWithIcon>(
    () => ({
      icon: collection?.icon?.url || '',
      name: collection?.name || '',
      summary: collection?.summary || ''
    }),
    [collection]
  );

  const formik = useFormik<EditCollectionFormValuesWithIcon>({
    enableReinitialize: true,
    onSubmit: vals => {
      return edit({
        collection: {
          name: vals.name,
          summary: vals.summary || undefined
        },
        icon: vals.icon
      }).then(done);
    },
    validationSchema,
    initialValues
  });
  const IconElement = (
    <DropzoneAreaCtrl
      imageOnly
      imageUrl={collection?.icon?.url}
      onFileSelected={icon => formik.setValues({ ...formik.values, icon }, false)}
    />
  );
  return <EditCollectionPanel IconElement={IconElement} cancel={done} formik={formik} />;
};
