import { TestUrlOrFile } from 'ctrl/lib/formik-validations';
import { useShareLinkFetchWebMetaMutation } from 'fe/resource/shareLink/useShareLink.generated';
import { useFormik } from 'formik';
import Maybe from 'graphql/tsutils/Maybe';
import { ResourceInput } from 'graphql/types.generated';
import { DOMAIN_REGEX, grades, languages, subjects, types, licenses, formats } from 'mn-constants';
import React, { FC, useRef, useMemo, useContext } from 'react';
import { ShareLink } from 'ui/modules/ShareLink';
import Fetched, { ShareResourceFormValues } from 'ui/modules/ShareLink/fetched';
import * as Yup from 'yup';
import { DropzoneAreaCtrl } from '../DropzoneArea/DropzoneAreaCtrl';
import {
  emptyCategorizationInitialValues,
  ResourceCategorizationCtrl,
  ResourceCategorizationType,
  categorizationSchema
} from '../ResourceCategorization/ResourceCategorizationCtrl';
import { ResourcePreviewFragment } from '../previews/resource/ResourcePreview.generated';
import { getCatOption, getLangOption } from 'mn-constants/categorizations/helpers';
import { LocaleContext } from 'context/global/localizationCtx';
import { getLicenseOption } from 'mn-constants/categorizations/licenses/helpers';
import { getFormatOption } from 'mn-constants/categorizations/format/helpers';

export interface ShareResourceFormValuesWithIconAndCategorization
  extends ShareResourceFormValues,
    ResourceCategorizationType {
  icon: File | string | undefined;
}
export const validationSchema = Yup.object<ShareResourceFormValuesWithIconAndCategorization>({
  ...categorizationSchema,
  name: Yup.string()
    .max(90)
    .required(),
  summary: Yup.string().max(1000),
  icon: Yup.mixed<File | string>().test(...TestUrlOrFile)
});

export const shareResourceInitialEmptyValues: ShareResourceFormValuesWithIconAndCategorization = {
  ...emptyCategorizationInitialValues,
  name: '',
  summary: '',
  icon: ''
};
export const fetchLinkInitialValues = {
  fetchUrl: ''
};

export interface ShareLinkResponse {
  resource: ResourceInput;
  icon: Maybe<string | File>;
  content: string;
}
export interface ShareLinkCtrl {
  done(_: ShareLinkResponse | null): Promise<any>;
  edit?: ResourcePreviewFragment;
}

export const ShareLinkCtrl: FC<ShareLinkCtrl> = ({ done, edit }: ShareLinkCtrl) => {
  const { i18n } = useContext(LocaleContext);
  const [fetchWebMeta, webMetaDataResult] = useShareLinkFetchWebMetaMutation();
  const webMetaData = webMetaDataResult.data?.fetchWebMetadata;
  const iconUploadPath = edit?.icon?.upload?.path?.split('/')[1] || undefined;

  const initialValues = useMemo<ShareResourceFormValuesWithIconAndCategorization>(
    () =>
      edit
        ? {
            icon: edit.icon?.url || '',
            summary: edit.summary || '',
            grade: getCatOption(grades, edit.level)!,
            format: getFormatOption(formats, edit.format)!,
            language: getLangOption(languages, edit.language)!,
            name: edit.name,
            subject: getCatOption(subjects, edit.subject)!,
            type: getCatOption(types, edit.type)!,
            contentUrl: edit.payload?.url || '',
            resource: undefined,
            license: getLicenseOption(licenses, edit.license)!
          }
        : webMetaData
        ? {
            ...emptyCategorizationInitialValues,
            icon: webMetaData.image || '',
            name: webMetaData.title || '',
            summary: webMetaData.summary || ''
          }
        : shareResourceInitialEmptyValues,
    [edit, webMetaData]
  );
  const { current: urlValidationSchema } = useRef(
    Yup.object<{ fetchUrl: string }>({
      fetchUrl: Yup.string()
        .matches(DOMAIN_REGEX)
        .required(i18n._('url is required'))
    })
  );

  const fetchFormik = useFormik<{ fetchUrl: string }>({
    initialValues: fetchLinkInitialValues,
    enableReinitialize: true,
    validationSchema: urlValidationSchema,
    onSubmit: ({ fetchUrl }) => fetchWebMeta({ variables: { url: fetchUrl } })
  });

  const shareFormik = useFormik<ShareResourceFormValuesWithIconAndCategorization>({
    initialValues,
    validationSchema,
    enableReinitialize: true,
    validateOnChange: false,
    onSubmit: vals => {
      const { icon, name, summary, grade, language, subject, type, license, format } = vals;
      const content = edit?.payload?.url || webMetaData?.url || fetchFormik.values.fetchUrl;
      if (!content) {
        return;
      }
      //const catValues = getCategorizationValues(vals);
      const resource: ResourceInput = {
        name: name,
        summary: summary,
        language: language.value,
        subject: subject.value,
        type: type.value,
        level: grade.value,
        license: license.value,
        format: format.value
      };

      return done({
        icon,
        resource,
        content
      });
    }
  });

  const IconElement = (
    <DropzoneAreaCtrl
      imageOnly
      imageUrl={edit?.icon?.url}
      fileName={iconUploadPath}
      onFileSelected={icon => shareFormik.setValues({ ...shareFormik.values, icon })}
    />
  );
  const CategorizationPanel = <ResourceCategorizationCtrl formik={shareFormik} />;

  return edit ? (
    <Fetched
      IconElement={IconElement}
      cancel={() => done(null)}
      formik={shareFormik}
      CategorizationPanel={CategorizationPanel}
    />
  ) : (
    <ShareLink
      IconElement={IconElement}
      FetchLinkFormik={fetchFormik}
      cancelFetched={() => done(null)}
      formik={shareFormik}
      isFetched={!!webMetaData}
      CategorizationPanel={CategorizationPanel}
    />
  );
};
