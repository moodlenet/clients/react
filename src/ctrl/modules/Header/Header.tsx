import { useMe } from 'fe/session/useMe';
import { SideBarContext } from 'ctrl/context/SideBar';
import { useNotifyMustLogin } from 'ctrl/lib/notifyMustLogin';
import { CreateCommunityPanelCtrl } from 'ctrl/modules/CreateCommunityPanel/createCommunityPanelCtrl';
import { SearchBox } from 'ctrl/modules/SearchBox/SearchBox';
import React, { FC, useContext, useMemo, useReducer } from 'react';
import { MainHeader, Props as MainHeaderProps } from 'ui/modules/MainHeader';
import Modal from 'ui/modules/Modal';
import { homeLocation } from 'routes/HomePageRoute';
import { userLocation } from 'routes/UserPageRoute';

export interface MainHeaderCtrl {}
export const MainHeaderCtrl: FC<MainHeaderCtrl> = () => {
  const meQ = useMe();
  const user = meQ.me?.user;
  const notifiedMustLogin = useNotifyMustLogin();

  const [showCreateCommunity, toggleShowCreateCommunity] = useReducer(
    is => (notifiedMustLogin() ? false : !is),
    false
  );

  const CreateCommunityModal = showCreateCommunity ? (
    <Modal closeModal={toggleShowCreateCommunity}>
      <CreateCommunityPanelCtrl done={toggleShowCreateCommunity} />
    </Modal>
  ) : null;

  const { toggleOpen: toggleSideBar } = useContext(SideBarContext);

  const [isOpenDropdown, toggleDropdown] = useReducer(is => !is, false);
  const headerProps = useMemo<MainHeaderProps>(() => {
    const props: MainHeaderProps = {
      Search: <SearchBox key="search" />,
      user: user
        ? {
            isAdmin: meQ.isAdmin,
            logout: meQ.logout,
            icon: user.icon?.url || '',
            link: userLocation.getPath({ userId: user.id, tab: undefined }, undefined),
            name: user.name || ''
          }
        : null,
      homeLink: homeLocation.getPath(undefined, undefined),
      toggleSideBar,
      createCommunity: toggleShowCreateCommunity,
      isOpenDropdown,
      toggleDropdown
    };
    return props;
  }, [user, meQ.isAdmin, meQ.logout, toggleSideBar, isOpenDropdown]);
  return (
    <>
      {CreateCommunityModal}
      <MainHeader {...headerProps} />
    </>
  );
};
