import { TestUrlOrFile } from 'ctrl/lib/formik-validations';
import { useFormik } from 'formik';
import Maybe from 'graphql/tsutils/Maybe';
import { ResourceInput } from 'graphql/types.generated';
import { grades, languages, subjects, types, formats } from 'mn-constants';
import { getFormatOption } from 'mn-constants/categorizations/format/helpers';
import { getCatOption, getLangOption } from 'mn-constants/categorizations/helpers';
import licenses from 'mn-constants/categorizations/licenses';
import { getLicenseOption } from 'mn-constants/categorizations/licenses/helpers';
import React, { FC, useEffect, useMemo } from 'react';
import { ResourceFormValues, UploadResource } from 'ui/modules/AddResource/UploadResource';
import { DropzoneArea } from 'ui/modules/DropzoneArea';
import * as Yup from 'yup';
import { DropzoneAreaCtrl, isImageFile } from '../DropzoneArea/DropzoneAreaCtrl';
import { ResourcePreviewFragment } from '../previews/resource/ResourcePreview.generated';
import {
  categorizationSchema,
  emptyCategorizationInitialValues,
  ResourceCategorizationCtrl,
  ResourceCategorizationType
} from '../ResourceCategorization/ResourceCategorizationCtrl';

export interface ExtendedResourceFormValues extends ResourceFormValues, ResourceCategorizationType {
  icon: File | string | undefined;
  resource: File | undefined;
}

export const validationSchema: Yup.ObjectSchema<ExtendedResourceFormValues> = Yup.object<
  ExtendedResourceFormValues
>({
  ...categorizationSchema,
  name: Yup.string()
    .max(90)
    .required('title is required'),
  summary: Yup.string().max(1000),
  icon: Yup.mixed<File | string>().test(...TestUrlOrFile),
  resource: Yup.mixed<File>().required()
});

export const emptyInitialValues: ExtendedResourceFormValues = {
  ...emptyCategorizationInitialValues,
  name: '',
  summary: '',
  icon: '',
  resource: undefined
};
export interface AddResourceResponse {
  resource: ResourceInput;
  icon: Maybe<string | File>;
  content: File | string;
}
export interface AddResourceCtrl {
  done(_: AddResourceResponse | null): any;
  edit?: ResourcePreviewFragment;
}

export const AddResourceCtrl: FC<AddResourceCtrl> = ({ done, edit }) => {
  const initialValues = useMemo<ExtendedResourceFormValues>(
    () =>
      edit
        ? {
            icon: edit.icon?.url || '',
            summary: edit.summary || '',
            grade: getCatOption(grades, edit.level)!,
            language: getLangOption(languages, edit.language)!,
            format: getFormatOption(formats, edit.format)!,
            name: edit.name,
            subject: getCatOption(subjects, edit.subject)!,
            type: getCatOption(types, edit.type)!,
            contentUrl: edit.payload?.url || '',
            resource: new File([], ''),
            license: getLicenseOption(licenses, edit.license)!
          }
        : emptyInitialValues,
    [edit]
  );

  const formik = useFormik<ExtendedResourceFormValues>({
    validationSchema,
    initialValues,
    enableReinitialize: true,
    validateOnBlur: false,
    validateOnChange: false,
    onSubmit: vals => {
      const {
        resource: content,
        icon,
        name,
        license,
        summary,
        grade,
        language,
        subject,
        type,
        format
      } = vals;
      if (!content) {
        return;
      }
      // const catValues = getCategorizationValues(vals);

      const resource: ResourceInput = {
        name,
        summary,
        license: license.value,
        language: language.value,
        level: grade.value,
        subject: subject.value,
        format: format.value,
        type: type.value
      };
      done({ resource, icon, content });
    }
  });
  const editIsImageResource = edit?.payload?.mediaType === 'image/jpeg';
  const editUploadPath = edit?.payload?.upload?.path?.split('/')[1] || undefined;
  const iconUploadPath = edit?.icon?.upload?.path?.split('/')[1] || undefined;
  const ResourceElement = edit?.payload?.upload ? (
    <DropzoneArea
      isImage={editIsImageResource}
      currentFilename={editUploadPath}
      currentImageUrl={editIsImageResource ? edit.payload.url : undefined}
    />
  ) : (
    <DropzoneAreaCtrl
      error={formik.errors.resource}
      onFileSelected={resource => {
        formik.setValues({ ...formik.values, resource }, false);
      }}
    />
  );
  const hideIconField = edit
    ? editIsImageResource
    : !formik.values.resource || isImageFile(formik.values.resource);

  useEffect(() => {
    hideIconField &&
      formik.values.icon &&
      formik.setValues({ ...formik.values, icon: undefined }, false);
  }, [formik, hideIconField]);

  const IconElement = (
    <DropzoneAreaCtrl
      onFileSelected={icon => {
        formik.setValues({ ...formik.values, icon }, false);
      }}
      imageUrl={edit?.icon?.url}
      fileName={iconUploadPath}
      imageOnly
    />
  );
  const CategorizationPanel = <ResourceCategorizationCtrl formik={formik} />;
  // console.table(formik.errors);
  // console.table(formik.values);
  return (
    <UploadResource
      hideIconField={hideIconField}
      cancel={() => done(null)}
      formik={formik}
      IconElement={IconElement}
      ResourceElement={ResourceElement}
      CategorizationPanel={CategorizationPanel}
    />
  );
};
