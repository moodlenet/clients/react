import { useCreateCollection } from 'fe/collection/create/useCreateCollection';
import { useFormik } from 'formik';
import React, { FC } from 'react';
import {
  BasicCreateCollectionFormValues,
  CreateCollectionPanel
} from 'ui/modules/CreateCollectionPanel';
import * as Yup from 'yup';
import { useHistory } from 'react-router-dom';
import { TestUrlOrFile } from 'ctrl/lib/formik-validations';
import { DropzoneAreaCtrl } from '../DropzoneArea/DropzoneAreaCtrl';
import { collectionLocation } from 'routes/CollectionPageRoute';

interface CreateCollectionFormValuesWithIcon extends BasicCreateCollectionFormValues {
  icon: string | File | undefined;
}
export const validationSchema: Yup.ObjectSchema<CreateCollectionFormValuesWithIcon> = Yup.object<
  CreateCollectionFormValuesWithIcon
>({
  name: Yup.string()
    .min(2)
    .max(60)
    .required(),
  summary: Yup.string().max(500),
  icon: Yup.mixed<string | File>().test(...TestUrlOrFile)
});
export interface Props {
  communityId: string;
  done(): any;
}
export const CreateCollectionPanelCtrl: FC<Props> = ({ communityId, done }: Props) => {
  const history = useHistory();
  const { create } = useCreateCollection(communityId);

  const formik = useFormik<CreateCollectionFormValuesWithIcon>({
    initialValues: {
      name: '',
      summary: '',
      icon: ''
    },
    enableReinitialize: true,
    onSubmit: vals => {
      return create({
        collection: {
          preferredUsername: vals.name.split(' ').join('_'),
          name: vals.name,
          summary: vals.summary
        },
        icon: vals.icon
      })
        .then(
          res =>
            res?.data?.createCollection?.id &&
            history.push(
              collectionLocation.getPath(
                { collectionId: res.data.createCollection.id, tab: undefined },
                undefined
              )
            )
        )
        .catch(err => console.log(err));
    },
    validationSchema
  });

  const IconElement = (
    <DropzoneAreaCtrl
      onFileSelected={icon => formik.setValues({ ...formik.values, icon }, false)}
      imageOnly
    />
  );

  return <CreateCollectionPanel IconElement={IconElement} cancel={done} formik={formik} />;
};
