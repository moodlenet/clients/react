import { useCreateCommunity } from 'fe/community/create/useCreateCommunity';
import { useFormik } from 'formik';
import React, { FC } from 'react';
import { useHistory } from 'react-router';
import { CreateCommunityFormValues, CreateCommunityPanel } from 'ui/modules/CreateCommunityPanel';
import * as Yup from 'yup';
import { TestUrlOrFile } from 'ctrl/lib/formik-validations';
import { communityLocation } from 'routes/CommunityPageRoute';
import { DropzoneAreaCtrl } from '../DropzoneArea/DropzoneAreaCtrl';

interface CommunityFormValuesWithIcon extends CreateCommunityFormValues {
  icon: string | File | undefined;
}
export const validationSchema: Yup.ObjectSchema<CommunityFormValuesWithIcon> = Yup.object<
  CommunityFormValuesWithIcon
>({
  name: Yup.string()
    .min(2)
    .max(60)
    .required(),
  summary: Yup.string().max(500),
  icon: Yup.mixed<File | string>().test(...TestUrlOrFile)
});
export interface Props {
  done(): any;
}
export const CreateCommunityPanelCtrl: FC<Props> = ({ done }: Props) => {
  const history = useHistory();
  const { create } = useCreateCommunity();
  const formik = useFormik<CommunityFormValuesWithIcon>({
    enableReinitialize: true,
    validationSchema,
    initialValues: {
      name: '',
      summary: '',
      icon: ''
    },
    onSubmit: vals => {
      return create({
        community: {
          preferredUsername: vals.name.split(' ').join('_'),
          name: vals.name,
          summary: vals.summary
        },
        icon: vals.icon
      }).then(res => {
        done();
        const gotoPath =
          res?.data?.createCommunity?.id &&
          communityLocation.getPath(
            { communityId: res.data.createCommunity.id, tab: undefined },
            undefined
          );
        gotoPath && history.push(gotoPath);
      });
    }
  });
  const IconElement = (
    <DropzoneAreaCtrl
      imageOnly
      onFileSelected={icon => formik.setValues({ ...formik.values, icon }, false)}
    />
  );
  return <CreateCommunityPanel IconElement={IconElement} cancel={done} formik={formik} />;
};
