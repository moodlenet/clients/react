import { t } from '@lingui/macro';
import { LocaleContext } from 'context/global/localizationCtx';
import React, { useCallback, useContext, useRef, useState } from 'react';
import SocialText from 'ui/modules/SocialText';
// const PickerWrap = styled.div`
//   position: absolute;
//   right: 10px;
//   top: 45px;
//   z-index: 999999999999999999;
// `;
export interface SocialTextCtrl {
  textChange(text: string): void;
  submit(text: string): void;
  defaultValue?: string;
  keepTextOnSubmit?: boolean;
  placeholder?: string;
}
const replyPlaceholder = t`Post a reply`;

export const SocialTextCtrl: React.FC<SocialTextCtrl> = ({
  submit,
  textChange,
  defaultValue = '',
  keepTextOnSubmit = false,
  placeholder
}) => {
  const { i18n } = useContext(LocaleContext);

  placeholder = placeholder || i18n._(replyPlaceholder);
  const [text, setText] = useState(defaultValue);
  // const onChangeRef = useRef(onChange);
  // onChangeRef.current = onChange;
  // useEffect(() => {
  //   onChangeRef.current(text);
  // }, [text]);
  // const [isEmojiOpen, setEmojiOpen] = useState(false);
  // const toggleEmoji = useCallback(() => setEmojiOpen(!isEmojiOpen), [
  //   isEmojiOpen
  // ]);
  // const addEmoji = React.useCallback(
  //   (code, obj) => {
  //     // console.log(code, obj);
  //     if (!ref.current) {
  //       return;
  //     }
  //     const textarea = ref.current as HTMLTextAreaElement;
  //     const selectionStart = textarea.selectionStart;
  //     // const selectionEnd = textarea.selectionEnd
  //     const offset = dropEmoji(textarea, obj.emoji);
  //     const pos = selectionStart + offset;
  //     textarea.focus();
  //     // console.log([selectionStart,selectionEnd], offset, pos, [textarea.selectionStart, textarea.selectionEnd] )
  //     textarea.selectionEnd = pos;
  //   },
  //   [ref.current]
  // );
  const submitref = useRef(submit);
  submitref.current = submit;
  const handleSubmit = useCallback(() => {
    submitref.current(text);
    if (!keepTextOnSubmit) {
      setText('');
    }
  }, [text, keepTextOnSubmit]);

  const submitUi = text ? handleSubmit : undefined;

  const textchangeref = useRef(textChange);
  textchangeref.current = textChange;
  const onChange = useCallback((ev: React.FormEvent<HTMLTextAreaElement>) => {
    const _text = ev.currentTarget.value;
    setText(_text);
    textchangeref.current(_text);
  }, []);

  const textAreaProps: SocialText['textAreaProps'] = {
    onChange,
    placeholder,
    value: text
  };

  return <SocialText textAreaProps={textAreaProps} submit={submitUi} />;
};
