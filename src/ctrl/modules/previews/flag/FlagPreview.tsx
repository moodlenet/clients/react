import { t } from '@lingui/macro';
import { i18nMark } from '@lingui/react';
import { LocaleContext } from 'context/global/localizationCtx';
import { useFlagPreview } from 'fe/flags/preview/useFlagPreview';
import { getActivityActor } from 'fe/lib/activity/getActivityActor';
import { getCommunityInfoStrings } from 'fe/lib/activity/getContextCommunityInfo';
import { Flag } from 'graphql/types.generated';
import React, { FC, useCallback, useContext, useMemo, useState } from 'react';
import { threadLocation } from 'routes/ThreadPageRoute';
import { ActivityPreview, Status } from 'ui/modules/ActivityPreview';
import { ConfirmationPanel } from 'ui/modules/ConfirmationPanel';
import Modal from 'ui/modules/Modal';
import { FlaggedItem, FlaggedProps } from 'ui/modules/Previews/FlaggedItem';
import { PreviewComponent } from '../activity/PreviewComponent';
import { CommentPreviewCtrl } from '../comment/CommentPreview';

interface FlagPreviewCtrl {
  flagId: Flag['id'];
}

type ConfirmType = 'delete' | 'block' | 'ignore';

const i18nTemplates = {
  delete: t`Delete {actorType}`,
  ignore: t`Ignore {actorType}`,
  deleteContentConfirm: t`Are you sure you want to permanently delete this {actorType} ?`
};
const i18nLabels = {
  ignore: i18nMark(`Ignore`),
  delete: i18nMark(`Delete`),
  deleteFlaggedContent: i18nMark(`Delete flagged content`)
};
const i18nActorTypes = {
  flag: i18nMark('Flag'),
  content: i18nMark('Content'),
  Collection: i18nMark('Collection'),
  Comment: i18nMark('Comment'),
  Community: i18nMark('Community'),
  Resource: i18nMark('Resource'),
  User: i18nMark('User')
};

export const FlagPreviewCtrl: FC<FlagPreviewCtrl> = ({ flagId }) => {
  const { i18n } = useContext(LocaleContext);
  const {
    deactivateFlaggedUser,
    deleteFlagContext,
    flag,
    ignoreFlag,
    deleteFlagContextStatus,
    deactivateFlaggedUserStatus,
    ignoreFlagStatus
  } = useFlagPreview(flagId);

  const [confirmType, setConfirmType] = useState<ConfirmType>();
  const closeConfirm = useCallback(() => setConfirmType(undefined), []);
  const ConfirmActionModal = !(flag && confirmType) ? null : (
    <Modal closeModal={closeConfirm}>
      {confirmType === 'delete' ? (
        <ConfirmationPanel
          cancel={closeConfirm}
          confirm={() => deleteFlagContext().then(closeConfirm)}
          waiting={deleteFlagContextStatus.loading || ignoreFlagStatus.loading}
          action={i18nLabels.deleteFlaggedContent}
          description={i18n._({
            ...i18nTemplates.deleteContentConfirm,
            values: { actorType: i18nActorTypes[flag.context.__typename] }
          })}
          title={i18nLabels.delete}
        />
      ) : confirmType === 'block' ? (
        <ConfirmationPanel
          cancel={closeConfirm}
          confirm={() => deactivateFlaggedUser().then(closeConfirm)}
          waiting={deactivateFlaggedUserStatus.loading}
          action={i18n._({ ...i18nTemplates.delete, values: { actorType: i18nActorTypes.User } })}
          description={i18n._({
            ...i18nTemplates.deleteContentConfirm,
            values: { actorType: i18nActorTypes.User }
          })}
          title={i18nLabels.delete}
        />
      ) : confirmType === 'ignore' ? (
        <ConfirmationPanel
          cancel={closeConfirm}
          confirm={() => ignoreFlag().then(closeConfirm)}
          waiting={deleteFlagContextStatus.loading}
          action={i18n._({ ...i18nTemplates.ignore, values: { actorType: i18nActorTypes.flag } })}
          description={i18n._({
            ...i18nTemplates.deleteContentConfirm,
            values: { actorType: i18nActorTypes.flag }
          })}
          title={i18nLabels.ignore}
        />
      ) : null // never
      }
    </Modal>
  );

  const FlaggedItemContextElement: FlaggedProps['FlaggedItemContextElement'] = useMemo(() => {
    if (!flag) {
      return <></>;
    } else if (flag.context.__typename === 'Comment') {
      const comment = flag.context;
      const { communityLink, communityName } = getCommunityInfoStrings(comment);
      const CommentPreview = (
        <CommentPreviewCtrl commentId={comment.id} mainComment={false} hideActions={true} />
      );
      const actor = flag.creator && getActivityActor(flag.creator);
      return (
        <ActivityPreview
          actor={actor}
          commentActor={comment.creator ? getActivityActor(comment.creator) : undefined}
          createdAt={comment.createdAt}
          event={'commented'}
          communityLink={communityLink}
          communityName={communityName}
          status={Status.Loaded}
          threadUrl={
            comment.thread?.id && threadLocation.getPath({ threadId: comment.thread.id }, undefined)
          }
          preview={CommentPreview}
        />
      );
    } else {
      return flag ? <PreviewComponent context={flag.context} flagged={true} /> : <></>;
    }
  }, [flag]);

  const props = useMemo<FlaggedProps | null>(() => {
    return flag
      ? {
          FlaggedItemContextElement,
          reason: flag.message,
          type: flag.context.__typename,
          blockUser: () => setConfirmType('block'),
          deleteContent: () => setConfirmType('delete'),
          ignoreFlag: () => setConfirmType('ignore')
        }
      : null;
  }, [FlaggedItemContextElement, flag]);
  return (
    props && (
      <>
        {ConfirmActionModal}
        <FlaggedItem {...props} />
      </>
    )
  );
};
