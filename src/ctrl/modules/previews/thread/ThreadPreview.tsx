import { useThreadPreview } from 'fe/thread/preview/useThreadPreview';
import { Thread } from 'graphql/types.generated';
import React, { FC, useMemo } from 'react';
import {
  CommentProps as ThreadPreviewUIProps,
  Thread as ThreadPreviewUI
} from 'ui/modules/Previews/Thread';
import { threadLocation } from 'routes/ThreadPageRoute';

export interface Props {
  threadId: Thread['id'];
}

export const ThreadPreviewCtrl: FC<Props> = ({ threadId }) => {
  const { mainComment, totalReplies } = useThreadPreview(threadId);

  const threadPreviewProps = useMemo<ThreadPreviewUIProps | null>(() => {
    if (!mainComment) {
      return null;
    }

    const props: ThreadPreviewUIProps = {
      content: mainComment.content,
      // lastActivity: lastActivity ? `${lastActivity}` : '',
      lastActivity: mainComment.createdAt,
      members: [],
      totalLikes: `${mainComment.likerCount || 0}`,
      totalReplies: totalReplies ? `${totalReplies}` : '',
      link: threadLocation.getPath({ threadId }, undefined)
    };

    return props;
  }, [mainComment, totalReplies, threadId]);

  return threadPreviewProps && <ThreadPreviewUI {...threadPreviewProps} />;
};
