import React, { FC } from 'react';
import { ActivityContextPreviewFragment } from 'fe/lib/activity/types';
import { CollectionPreviewCtrl } from '../collection/CollectionPreview';
import { CommentPreviewCtrl } from '../comment/CommentPreview';
import { CommunityPreviewCtrl } from '../community/CommunityPreview';
import { ResourcePreviewCtrl } from '../resource/ResourcePreview';
import { UserPreviewCtrl } from '../user/UserPreview';
import { LikedCommentPreviewCtrl } from '../commentLiked/CommentLikedPreview';
import { getActivityMainContext } from 'fe/lib/activity/getActivityMainContext';

export const PreviewComponent: FC<{
  context: ActivityContextPreviewFragment;
  flagged?: boolean;
}> = ({ context, flagged }) => {
  if (context.__typename === 'Collection') {
    if (flagged) {
      return <CollectionPreviewCtrl collectionId={context.id} flagged={flagged} />;
    } else {
      return <CollectionPreviewCtrl collectionId={context.id} />;
    }
  } else if (context.__typename === 'Comment') {
    return <CommentPreviewCtrl commentId={context.id} mainComment={false} />;
  } else if (context.__typename === 'Community') {
    if (flagged) {
      return <CommunityPreviewCtrl communityId={context.id} flagged={flagged} />;
    } else {
      return <CommunityPreviewCtrl communityId={context.id} />;
    }
  } else if (context.__typename === 'Resource') {
    if (flagged) {
      return <ResourcePreviewCtrl resourceId={context.id} flagged={flagged} />;
    } else {
      return <ResourcePreviewCtrl resourceId={context.id} />;
    }
  } else if (context.__typename === 'User') {
    if (flagged) {
      return <UserPreviewCtrl userId={context.userId} flagged={flagged} />;
    } else {
      return <UserPreviewCtrl userId={context.userId} />;
    }
  } else {
    if (context.__typename === 'Like' && context.context?.__typename === 'Comment') {
      return <LikedCommentPreviewCtrl commentId={context.context.id} />;
    } else {
      const mainContext = getActivityMainContext(context);
      return mainContext ? <PreviewComponent context={mainContext} /> : null;
    }
  }
};
