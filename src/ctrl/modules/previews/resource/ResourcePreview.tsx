import { useNotifyMustLogin } from 'ctrl/lib/notifyMustLogin';
import { AddResourceCtrl, AddResourceResponse } from 'ctrl/modules/AddResource/addResourceCtrl';
import { FlagModalCtrl } from 'ctrl/modules/FlagModal/flagModalCtrl';
import { MoodlePanelCtrl } from 'ctrl/modules/MoodlePanel/MoodlePanel';
import { ShareLinkCtrl, ShareLinkResponse } from 'ctrl/modules/ShareLink/shareLinkCtrl';
import { resourceGql2lms } from 'fe/lib/moodleLMS/mappings/gql2LMS';
import { useResourcePreview } from 'fe/resource/preview/useResourcePreview';
import { useFormik } from 'formik';
import Maybe from 'graphql/tsutils/Maybe';
import { Resource } from 'graphql/types.generated';
import { getResourceCatDescs } from 'mn-constants/categorizations/helpers';
import React, { FC, useCallback, useMemo, useReducer, useContext } from 'react';
import { collectionLocation } from 'routes/CollectionPageRoute';
import Modal from 'ui/modules/Modal';
import {
  Props as ResourcePreviewProps,
  Resource as ResourcePreviewUI
} from 'ui/modules/Previews/Resource';
import { ResourcePreviewFragment } from './ResourcePreview.generated';
import { i18nMark } from '@lingui/react';
import { ConfirmationPanel } from 'ui/modules/ConfirmationPanel';
import { t } from '@lingui/macro';
import { LocaleContext } from 'context/global/localizationCtx';

export interface Props {
  resourceId: Resource['id'];
  flagged?: boolean;
}
const deleteResourceMsg = i18nMark(`Delete Resource`);
const deleteResourceMsgDesc = t`Permanently delete resource {name}?`;
const deleteResourceTitle = t`Delete resource {name}`;

export const ResourcePreviewCtrl: FC<Props> = ({ resourceId, flagged }) => {
  const { resource, toggleLike, canEdit, edit, canDelete, del, isDeleting } = useResourcePreview(
    resourceId
  );
  const { i18n } = useContext(LocaleContext);
  const toggleLikeFormik = useFormik({
    initialValues: {},
    onSubmit: toggleLike
  });
  const notifyMustLogin = useNotifyMustLogin();
  const [isOpenDropdown, toggleDropdown] = useReducer(is => !is, false);
  const [isOpenSendToMoodle, toggleSendToMoodleModal] = useReducer(is => !is, false);
  const [isOpenFlag, toggleFlagModal] = useReducer(is => (notifyMustLogin() ? false : !is), false);
  const [isOpenDelete, toggleDeleteModal] = useReducer(is => canDelete && !is, false);
  const [isOpenEdit, toggleEditModal] = useReducer(is => canEdit && !is, false);

  const resourcePreviewProps = useMemo<ResourcePreviewProps | null>(() => {
    if (!resource) {
      return null;
    }

    const hideActions = flagged ? true : false;
    return {
      ...resourceFragment2UIProps({
        resource,
        like: {
          iLikeIt: !!resource.myLike,
          toggleLikeFormik,
          totalLikes: resource.likerCount || 0
        }
      }),
      hideActions,
      isOpenDropdown,
      toggleDropdown,
      toggleEdit: canEdit ? toggleEditModal : null,
      toggleDelete: canDelete ? toggleDeleteModal : null,
      sendToMoodle: toggleSendToMoodleModal,
      toggleFlag: toggleFlagModal
    };
  }, [resource, flagged, toggleLikeFormik, isOpenDropdown, canEdit, canDelete]);

  const FlagModal =
    isOpenFlag && resource ? (
      <Modal closeModal={toggleFlagModal}>
        <FlagModalCtrl done={toggleFlagModal} ctx={resource} />
      </Modal>
    ) : null;

  const editResourceCb = useCallback(
    (resp: ShareLinkResponse | AddResourceResponse | null) => {
      return Promise.resolve(
        resp &&
          edit({
            resource: resp.resource,
            icon: resp.icon
          })
      ).then(toggleEditModal);
    },
    [edit]
  );

  const EditModal =
    isOpenEdit && resource ? (
      <Modal closeModal={toggleEditModal}>
        {isUpload(resource) ? (
          <AddResourceCtrl done={editResourceCb} edit={resource} />
        ) : (
          <ShareLinkCtrl done={editResourceCb} edit={resource} />
        )}
      </Modal>
    ) : null;

  const delResourceCb = useCallback(() => del().then(toggleDeleteModal), [del]);
  const DeleteModal =
    isOpenDelete && resource ? (
      <Modal closeModal={toggleDeleteModal}>
        <ConfirmationPanel
          cancel={toggleDeleteModal}
          confirm={delResourceCb}
          action={deleteResourceMsg}
          description={i18n._({ ...deleteResourceMsgDesc, values: resource })}
          title={i18n._({ ...deleteResourceTitle, values: resource })}
          waiting={isDeleting}
        />
      </Modal>
    ) : null;

  const resourceLMS = useMemo(() => resourceGql2lms(resource), [resource]);
  const MoodleModal =
    resourceLMS && isOpenSendToMoodle ? (
      <Modal closeModal={toggleSendToMoodleModal}>
        <MoodlePanelCtrl done={toggleSendToMoodleModal} resource={resourceLMS} />
      </Modal>
    ) : null;
  return (
    resourcePreviewProps && (
      <>
        {EditModal}
        {FlagModal}
        {DeleteModal}
        {MoodleModal}
        <ResourcePreviewUI {...resourcePreviewProps} />
      </>
    )
  );
};

export const resourceFragment2UIProps = (args: {
  resource: ResourcePreviewFragment;
  like: ResourcePreviewProps['like'];
}) => {
  const { like, resource } = args;
  const props: Omit<
    ResourcePreviewProps,
    | 'isOpenDropdown'
    | 'toggleDropdown'
    | 'sendToMoodle'
    | 'toggleFlag'
    | 'toggleDelete'
    | 'toggleEdit'
    | 'hideActions'
  > = {
    ...getResourceCatDescs(resource),
    icon: resource.icon?.url || '',
    link: resource.payload?.url || '',
    name: resource.name || '',
    summary: resource.summary || '',
    like,
    isFile: isUpload(resource),
    //acceptedLicenses: accepted_license_types,
    license: resource.license || null,
    isFlagged: !!resource.myFlag,
    //FlagModal,
    // sendToMoodle,
    // MoodlePanel,
    // type: resource.payload?.mediaType,
    collectionName: resource.collection?.name || '',
    collectionLink: collectionLocation.getPath(
      { collectionId: resource.collection?.id || '', tab: undefined },
      undefined
    )
  };
  return props;
};

const isUpload = (_: Maybe<ResourcePreviewFragment>) => !!_?.payload?.upload;
