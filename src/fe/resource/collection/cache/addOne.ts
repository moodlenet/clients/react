import { DataProxy } from 'apollo-cache';
import { Collection } from 'graphql/types.generated';
import {
  CollectionResourceFragment,
  CollectionResourcesDocument,
  CollectionResourcesQuery,
  CollectionResourcesQueryVariables
} from '../useCollectionResources.generated';

type Data = {
  resource: CollectionResourceFragment;
  collectionId: Collection['id'];
};

export const addOne = (proxy: DataProxy, { collectionId, resource }: Data) => {
  const cache = proxy.readQuery<CollectionResourcesQuery, CollectionResourcesQueryVariables>({
    variables: { collectionId },
    query: CollectionResourcesDocument
  });
  if (!cache?.collection?.resources?.edges) {
    return;
  }
  const oldEdges = cache.collection.resources.edges;
  const edges = [resource, ...oldEdges];
  proxy.writeQuery<CollectionResourcesQuery, CollectionResourcesQueryVariables>({
    query: CollectionResourcesDocument,
    variables: { collectionId },
    data: {
      ...cache,
      collection: {
        ...cache.collection,
        resources: { ...cache.collection.resources, edges }
      }
    }
  });
};
