import { DataProxy } from 'apollo-cache';
import { Collection, Follow } from 'graphql/types.generated';
import {
  CollectionFollowersDocument,
  CollectionFollowersQuery,
  CollectionFollowersQueryVariables
} from '../useCollectionFollowers.generated';

export type Data = {
  followId: Follow['id'];
  collectionId: Collection['id'];
};

export const removeOne = (proxy: DataProxy, { collectionId, followId }: Data) => {
  const cache = proxy.readQuery<CollectionFollowersQuery, CollectionFollowersQueryVariables>({
    query: CollectionFollowersDocument,
    variables: { collectionId }
  });
  if (!cache?.collection?.followers?.edges) {
    return;
  }
  const newEdges = cache.collection.followers.edges.filter(edge => edge.id !== followId);
  proxy.writeQuery<CollectionFollowersQuery, CollectionFollowersQueryVariables>({
    query: CollectionFollowersDocument,
    variables: { collectionId },
    data: {
      ...cache,
      collection: {
        ...cache.collection,
        followers: {
          ...cache.collection.followers,
          edges: newEdges
        }
      }
    }
  });
};
