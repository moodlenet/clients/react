import { DataProxy } from 'apollo-cache';
import { Collection } from 'graphql/types.generated';
import {
  CollectionFollowEdgeFragment,
  CollectionFollowersDocument,
  CollectionFollowersQuery,
  CollectionFollowersQueryVariables
} from '../useCollectionFollowers.generated';

export type Data = {
  follower: CollectionFollowEdgeFragment;
  collectionId: Collection['id'];
};

export const addOne = (proxy: DataProxy, { collectionId, follower }: Data) => {
  const cache = proxy.readQuery<CollectionFollowersQuery, CollectionFollowersQueryVariables>({
    query: CollectionFollowersDocument,
    variables: { collectionId }
  });
  if (!cache?.collection?.followers?.edges) {
    return;
  }
  const oldEdges = cache.collection.followers.edges;
  const newEdges: CollectionFollowEdgeFragment[] = [follower, ...oldEdges];
  proxy.writeQuery<CollectionFollowersQuery, CollectionFollowersQueryVariables>({
    query: CollectionFollowersDocument,
    variables: { collectionId },
    data: {
      ...cache,
      collection: {
        ...cache.collection,
        followers: {
          ...cache.collection.followers,
          edges: newEdges
        }
      }
    }
  });
};
