import { DataProxy } from 'apollo-cache';
import { Community, Follow } from 'graphql/types.generated';
import {
  CommunityFollowersDocument,
  CommunityFollowersQuery,
  CommunityFollowersQueryVariables
} from '../useCommunityFollowers.generated';

export type Data = {
  followId: Follow['id'];
  communityId: Community['id'];
};

export const removeOne = (proxy: DataProxy, { communityId, followId }: Data) => {
  const cache = proxy.readQuery<CommunityFollowersQuery, CommunityFollowersQueryVariables>({
    query: CommunityFollowersDocument,
    variables: { communityId }
  });
  if (!cache?.community?.followers?.edges) {
    return;
  }
  const newEdges = cache.community.followers.edges.filter(edge => edge.id !== followId);
  proxy.writeQuery<CommunityFollowersQuery, CommunityFollowersQueryVariables>({
    query: CommunityFollowersDocument,
    variables: { communityId },
    data: {
      ...cache,
      community: {
        ...cache.community,
        followers: {
          ...cache.community.followers,
          edges: newEdges
        }
      }
    }
  });
};
