import { DataProxy } from 'apollo-cache';
import { Community } from 'graphql/types.generated';
import {
  CommunityFollowEdgeFragment,
  CommunityFollowersDocument,
  CommunityFollowersQuery,
  CommunityFollowersQueryVariables
} from '../useCommunityFollowers.generated';

export type Data = {
  follower: CommunityFollowEdgeFragment;
  communityId: Community['id'];
};

export const addOne = (proxy: DataProxy, { communityId, follower }: Data) => {
  const cache = proxy.readQuery<CommunityFollowersQuery, CommunityFollowersQueryVariables>({
    query: CommunityFollowersDocument,
    variables: { communityId }
  });
  if (!cache?.community?.followers?.edges) {
    return;
  }
  const oldEdges = cache.community.followers.edges;
  const newEdges: CommunityFollowEdgeFragment[] = [follower, ...oldEdges];
  proxy.writeQuery<CommunityFollowersQuery, CommunityFollowersQueryVariables>({
    query: CommunityFollowersDocument,
    variables: { communityId },
    data: {
      ...cache,
      community: {
        ...cache.community,
        followers: {
          ...cache.community.followers,
          edges: newEdges
        }
      }
    }
  });
};
