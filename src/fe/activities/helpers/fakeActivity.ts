import { ActivityPreviewFragment } from 'ctrl/modules/previews/activity/ActivityPreview.generated';
import { ActivityVerb } from 'graphql/types.generated';
import { NotMaybe } from 'util/data/notMaybe';
import { UseMeDataFragment } from 'fe/session/me.generated';
import { DateTime } from 'luxon';

type Context = NotMaybe<ActivityPreviewFragment['context']>;
const FAKE_PREFIX = '##FAKE_ACTIVITY_ID##';
const fakeID = () => `${FAKE_PREFIX}${new Date().valueOf().toString(36)}`;
export const isFakeId = (id: string) => id.startsWith(FAKE_PREFIX);

type Data = {
  context: Context;
  verb: ActivityVerb;
  user: UseMeDataFragment['user'] | NotMaybe<ActivityPreviewFragment['user']>;
};

// BEWARE: newly performed mutations on actors are inserted at top of existing
// activity-timeline-lists caches wrapped into a fake activityPreviewFragment
// at the moment Activities ID are never used in any operation, so this hach shouldn't
// generate problems.
// https://tracker.moodle.org/browse/MDLNET-493?focusedCommentId=813197&page=com.atlassian.jira.plugin.system.issuetabpanels%3Acomment-tabpanel#comment-813197
// FIXME whenever possible
export const fakeActivityPreviewFragment = ({
  context,
  verb,
  user
}: Data): ActivityPreviewFragment => {
  const _user: NotMaybe<ActivityPreviewFragment['user']> =
    'userId' in user
      ? user
      : {
          ...user,
          userId: user.id,
          userName: user.name
        };
  return {
    __typename: 'Activity',
    createdAt: DateTime.utc().toSQL(),
    id: fakeID(),
    user: _user,
    verb,
    context
  };
};
