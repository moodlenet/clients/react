import { DataProxy } from 'apollo-cache';
import {
  MyInboxActivitiesDocument,
  MyInboxActivitiesQuery,
  MyInboxActivitiesQueryVariables
} from '../useMyInboxActivities.generated';
import { ActivityPreviewFragment } from 'ctrl/modules/previews/activity/ActivityPreview.generated';

type Data = {
  activity: ActivityPreviewFragment;
};

export const addOne = (proxy: DataProxy, { activity }: Data) => {
  const cache = proxy.readQuery<MyInboxActivitiesQuery, MyInboxActivitiesQueryVariables>({
    query: MyInboxActivitiesDocument,
    variables: {}
  });
  if (!cache?.me?.user.inbox?.edges) {
    return;
  }
  const newEdges = [activity, ...cache.me.user.inbox.edges];
  proxy.writeQuery<MyInboxActivitiesQuery, MyInboxActivitiesQueryVariables>({
    query: MyInboxActivitiesDocument,
    variables: {},
    data: {
      ...cache,
      me: {
        ...cache.me,
        user: {
          ...cache.me.user,
          inbox: {
            ...cache.me.user.inbox,
            edges: newEdges
          }
        }
      }
    }
  });
};
