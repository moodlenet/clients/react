import { DataProxy } from 'apollo-cache';
import {
  CommunityOutboxActivitiesDocument,
  CommunityOutboxActivitiesQuery,
  CommunityOutboxActivitiesQueryVariables
} from '../useCommunityOutboxActivities.generated';
import { ActivityPreviewFragment } from 'ctrl/modules/previews/activity/ActivityPreview.generated';
import { Community } from 'graphql/types.generated';

type Data = {
  activity: ActivityPreviewFragment;
  communityId: Community['id'];
};

export const addOne = (proxy: DataProxy, { activity, communityId }: Data) => {
  const cache = proxy.readQuery<
    CommunityOutboxActivitiesQuery,
    CommunityOutboxActivitiesQueryVariables
  >({
    query: CommunityOutboxActivitiesDocument,
    variables: { communityId }
  });
  if (!cache?.community?.outbox?.edges) {
    return;
  }
  const newEdges = [activity, ...cache.community.outbox.edges];
  proxy.writeQuery<CommunityOutboxActivitiesQuery, CommunityOutboxActivitiesQueryVariables>({
    query: CommunityOutboxActivitiesDocument,
    variables: { communityId },
    data: {
      ...cache,
      community: {
        ...cache.community,
        outbox: {
          ...cache.community.outbox,
          edges: newEdges
        }
      }
    }
  });
};
