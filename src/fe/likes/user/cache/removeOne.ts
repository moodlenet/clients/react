import { DataProxy } from 'apollo-cache';
import { Like, User } from 'graphql/types.generated';
import {
  UserLikesDocument,
  UserLikesQuery,
  UserLikesQueryVariables
} from '../useUserLikes.generated';

export type Data = {
  userId: User['id'];
  likeId: Like['id'];
};
export const removeOne = (proxy: DataProxy, { likeId, userId }: Data) => {
  const cache = proxy.readQuery<UserLikesQuery, UserLikesQueryVariables>({
    query: UserLikesDocument,
    variables: { userId }
  });
  if (!cache?.user?.likes?.edges) {
    return;
  }
  const newEdges = cache.user.likes.edges.filter(like => like.id !== likeId);
  proxy.writeQuery<UserLikesQuery, UserLikesQueryVariables>({
    query: UserLikesDocument,
    variables: { userId },
    data: {
      ...cache,
      user: {
        ...cache.user,
        likes: {
          ...cache.user.likes,
          edges: newEdges
        }
      }
    }
  });
};
