import { DataProxy } from 'apollo-cache';
import {
  UserLikesDocument,
  UserLikesQuery,
  UserLikesQueryVariables
} from '../useUserLikes.generated';
import { User } from 'graphql/types.generated';
import { LikePreviewFragment } from 'ctrl/modules/previews/like/LikePreview.generated';

export type Data = {
  userId: User['id'];
  like: LikePreviewFragment;
};
export const addOrSubstituteOne = (proxy: DataProxy, { like, userId }: Data) => {
  const cache = proxy.readQuery<UserLikesQuery, UserLikesQueryVariables>({
    query: UserLikesDocument,
    variables: { userId }
  });
  if (!cache?.user?.likes?.edges) {
    return;
  }
  let newEdges = [...cache.user.likes.edges];
  const wasLikedIndex = cache.user.likes.edges.findIndex(
    _ =>
      (like.context.__typename === 'User' ? like.context.userId : like.context.id) ===
      (_.context.__typename === 'User' ? _.context.userId : _.context.id)
  );
  if (wasLikedIndex > -1) {
    newEdges[wasLikedIndex] = like;
  } else {
    newEdges = [like, ...newEdges];
  }
  proxy.writeQuery<UserLikesQuery, UserLikesQueryVariables>({
    query: UserLikesDocument,
    variables: { userId },
    data: {
      ...cache,
      user: {
        ...cache.user,
        likes: {
          ...cache.user.likes,
          edges: newEdges
        }
      }
    }
  });
};
