import { DataProxy } from 'apollo-cache';
import {
  UserFollowedCommunitiesDocument,
  UserFollowedCommunitiesQuery,
  UserFollowedCommunitiesQueryVariables,
  UserCommunityFollowFragment
} from '../useUserFollowedCommunities.generated';
import { User } from 'graphql/types.generated';

type Data = {
  communityFollow: UserCommunityFollowFragment;
  userId: User['id'];
};

export const addOrSubstituteOne = (proxy: DataProxy, { communityFollow, userId }: Data) => {
  const cache = proxy.readQuery<
    UserFollowedCommunitiesQuery,
    UserFollowedCommunitiesQueryVariables
  >({
    query: UserFollowedCommunitiesDocument,
    variables: { userId }
  });
  if (!cache?.user?.communityFollows?.edges) {
    return;
  }
  let newEdges = [...cache.user.communityFollows.edges];

  const wasFollowedIndex = cache.user.communityFollows.edges.findIndex(
    _ =>
      _.context.__typename === 'Community' &&
      communityFollow.context.__typename === 'Community' &&
      communityFollow.context.id === _.context.id
  );
  if (wasFollowedIndex > -1) {
    newEdges[wasFollowedIndex] = communityFollow;
  } else {
    newEdges = [communityFollow, ...newEdges];
  }
  proxy.writeQuery<UserFollowedCommunitiesQuery, UserFollowedCommunitiesQueryVariables>({
    query: UserFollowedCommunitiesDocument,
    variables: { userId },
    data: {
      ...cache,
      user: {
        ...cache.user,
        communityFollows: {
          ...cache.user.communityFollows,
          edges: newEdges
        }
      }
    }
  });
};
