import { useCallOrNotifyMustLogin } from 'ctrl/lib/notifyMustLogin';
import { fakeActivityPreviewFragment } from 'fe/activities/helpers/fakeActivity';
import * as myActivitiesInboxCache from 'fe/activities/inbox/my/cache';
import * as instanceActivitiesOutboxCache from 'fe/activities/outbox/instance/cache';
import * as userActivitiesOutboxCache from 'fe/activities/outbox/user/cache';
import * as allCommunitiesCache from 'fe/community/all/cache';
import * as userCommunityFollowsCache from 'fe/community/user/cache';
import * as myCommunityFollowsCache from 'fe/community/myFollowed/cache';
import * as userPageCache from 'fe/user/cache';
import { getMaybeUploadInput } from 'fe/mutation/upload/getUploadInput';
import { useMe } from 'fe/session/useMe';
import Maybe from 'graphql/tsutils/Maybe';
import { ActivityVerb, CommunityInput } from 'graphql/types.generated';
import { useMemo } from 'react';
import { useCreateCommunityMutation } from './useCreateCommunity.generated';

export interface CreateCommunity {
  community: CommunityInput;
  icon: Maybe<File | string>;
}
export const useCreateCommunity = () => {
  const [createMut, createMutStatus] = useCreateCommunityMutation();
  const { me } = useMe();

  const create = useCallOrNotifyMustLogin(
    async ({ community, icon }: CreateCommunity) => {
      if (createMutStatus.loading) {
        return;
      }

      return createMut({
        variables: {
          icon: getMaybeUploadInput(icon, null),
          community: {
            name: community.name,
            summary: community.summary,
            preferredUsername: community.preferredUsername
          }
        },
        update: (proxy, result) => {
          if (!(result.data?.createCommunity && me?.user)) {
            return;
          }
          const meUserId = me.user.id;

          const community = {
            ...result.data.createCommunity,
            threads: { totalCount: 0, __typename: 'ThreadsPage' } as const
          };

          const activity = fakeActivityPreviewFragment({
            context: community,
            user: me.user,
            verb: ActivityVerb.Created
          });

          instanceActivitiesOutboxCache.addOne(proxy, { activity });
          userActivitiesOutboxCache.addOne(proxy, { activity, userId: meUserId });
          myActivitiesInboxCache.addOne(proxy, { activity });

          allCommunitiesCache.addOne(proxy, { community });
          if (community.creator?.id) {
            userPageCache.addFollowsAmount(proxy, {
              amount: 1,
              on: 'Community',
              userId: community.creator.id
            });
          }
          if (community.creator?.id === meUserId) {
            if (community.myFollow) {
              const communityFollow = {
                ...community.myFollow,
                context: community
              };
              myCommunityFollowsCache.addOne(proxy, { communityFollow });
              userCommunityFollowsCache.addOrSubstituteOne(proxy, {
                userId: meUserId,
                communityFollow
              });
            }
          }
        }
      });
    },
    [createMutStatus, createMut]
  );
  return useMemo(() => {
    return {
      create
    };
  }, [create]);
};
