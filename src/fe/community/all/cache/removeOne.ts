import { DataProxy } from 'apollo-cache';
import {
  AllCommunitiesDocument,
  AllCommunitiesQuery,
  AllCommunitiesQueryVariables
} from '../useAllCommunities.generated';
import { CommunityPreviewFragment } from 'ctrl/modules/previews/community/CommunityPreview.generated';

type Data = {
  communityId: CommunityPreviewFragment['id'];
};

export const removeOne = (proxy: DataProxy, { communityId }: Data) => {
  const cache = proxy.readQuery<AllCommunitiesQuery, AllCommunitiesQueryVariables>({
    query: AllCommunitiesDocument
  });
  if (!cache?.communities.edges) {
    return;
  }
  const newEdges = cache.communities.edges.filter(follow => follow.id !== communityId);
  proxy.writeQuery<AllCommunitiesQuery, AllCommunitiesQueryVariables>({
    query: AllCommunitiesDocument,
    data: {
      ...cache,
      communities: {
        ...cache.communities,
        edges: newEdges
      }
    }
  });
};
