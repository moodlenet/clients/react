import { DataProxy } from 'apollo-cache';
import {
  AllCommunitiesDocument,
  AllCommunitiesQuery,
  AllCommunitiesQueryVariables
} from '../useAllCommunities.generated';
import { CommunityPreviewFragment } from 'ctrl/modules/previews/community/CommunityPreview.generated';

type Data = {
  community: CommunityPreviewFragment;
};

export const addOne = (proxy: DataProxy, { community }: Data) => {
  const cache = proxy.readQuery<AllCommunitiesQuery, AllCommunitiesQueryVariables>({
    query: AllCommunitiesDocument
  });
  if (!cache?.communities.edges) {
    return;
  }
  const oldEdges = cache.communities.edges;
  type Edges = typeof oldEdges;
  const newEdges: Edges = [community, ...oldEdges];
  proxy.writeQuery<AllCommunitiesQuery, AllCommunitiesQueryVariables>({
    query: AllCommunitiesDocument,
    data: {
      ...cache,
      communities: {
        ...cache.communities,
        edges: newEdges
      }
    }
  });
};
