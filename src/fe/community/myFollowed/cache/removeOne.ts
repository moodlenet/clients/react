import { DataProxy } from 'apollo-cache';
import {
  MyCommunityFollowsDataFragment,
  MyCommunityFollowsQuery,
  MyCommunityFollowsQueryVariables,
  MyCommunityFollowsDocument
} from '../myFollowedCommunities.generated';

type Data = {
  communityFollowId: MyCommunityFollowsDataFragment['id'];
};

export const removeOne = (proxy: DataProxy, { communityFollowId }: Data) => {
  const cache = proxy.readQuery<MyCommunityFollowsQuery, MyCommunityFollowsQueryVariables>({
    query: MyCommunityFollowsDocument
  });
  if (!cache?.me?.user.communityFollows?.edges) {
    return;
  }
  const edges = cache.me.user.communityFollows.edges.filter(
    follow => follow.id !== communityFollowId
  );
  proxy.writeQuery<MyCommunityFollowsQuery, MyCommunityFollowsQueryVariables>({
    query: MyCommunityFollowsDocument,
    data: {
      ...cache,
      me: {
        ...cache.me,
        user: {
          ...cache.me.user,
          communityFollows: {
            ...cache.me.user.communityFollows,
            edges
          }
        }
      }
    }
  });
};
