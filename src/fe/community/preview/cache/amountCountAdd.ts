import { DataProxy } from 'apollo-cache';
import {
  CommunityPreviewDocument,
  CommunityPreviewQueryVariables,
  CommunityPreviewQuery
} from '../useCommunityPreview.generated';
import { Community } from 'graphql/types.generated';
import { CommunityPreviewFragment } from 'ctrl/modules/previews/community/CommunityPreview.generated';

type OnField = 'collectionCount' | 'followerCount';
export type Data = {
  communityId: Community['id'];
  on: OnField;
  amount: number;
};

export const amountCountAdd = (proxy: DataProxy, { amount, on, communityId }: Data) => {
  const cache = proxy.readQuery<CommunityPreviewQuery, CommunityPreviewQueryVariables>({
    query: CommunityPreviewDocument,
    variables: { communityId }
  });
  if (!(cache?.community && amount)) {
    return;
  }

  const newAmount = (cache.community[on] || 0) + Math.ceil(amount);

  const newCommunity: CommunityPreviewFragment = {
    ...cache.community,
    [on]: newAmount
  };

  proxy.writeQuery<CommunityPreviewQuery, CommunityPreviewQueryVariables>({
    query: CommunityPreviewDocument,
    data: { ...cache, community: newCommunity },
    variables: { communityId }
  });
};
