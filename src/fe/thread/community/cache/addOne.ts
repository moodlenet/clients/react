import { DataProxy } from 'apollo-cache';
import { Community } from 'graphql/types.generated';
import {
  CommunityThreadFragment,
  CommunityThreadsDocument,
  CommunityThreadsQuery,
  CommunityThreadsQueryVariables
} from '../useCommunityThreads.generated';
type Data = {
  communityId: Community['id'];
  thread: CommunityThreadFragment;
};
export const addOne = (proxy: DataProxy, { communityId, thread }: Data) => {
  const cache = proxy.readQuery<CommunityThreadsQuery, CommunityThreadsQueryVariables>({
    query: CommunityThreadsDocument,
    variables: { communityId }
  });
  if (!cache?.community?.threads?.edges) {
    return;
  }
  const newEdges: CommunityThreadFragment[] = [thread, ...cache.community.threads.edges];
  proxy.writeQuery<CommunityThreadsQuery, CommunityThreadsQueryVariables>({
    query: CommunityThreadsDocument,
    variables: { communityId },
    data: {
      ...cache,
      community: {
        ...cache.community,
        threads: {
          ...cache.community.threads,
          edges: newEdges
        }
      }
    }
  });
};
