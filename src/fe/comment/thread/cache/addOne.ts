import { DataProxy } from 'apollo-cache';
import { Thread } from 'graphql/types.generated';
import {
  ThreadCommentEdgeFragment,
  ThreadCommentsDocument,
  ThreadCommentsQuery,
  ThreadCommentsQueryVariables
} from '../useThreadComments.generated';
type Data = {
  threadId: Thread['id'];
  comment: ThreadCommentEdgeFragment;
};
export const addOne = (proxy: DataProxy, { threadId, comment }: Data) => {
  const cache = proxy.readQuery<ThreadCommentsQuery, ThreadCommentsQueryVariables>({
    query: ThreadCommentsDocument,
    variables: { threadId }
  });
  if (
    !cache?.thread?.comments?.edges ||
    cache.thread.comments.pageInfo.hasNextPage // append to end only if we're on last page
  ) {
    return;
  }
  const newEdges: ThreadCommentEdgeFragment[] = [...cache.thread.comments.edges, comment];
  proxy.writeQuery<ThreadCommentsQuery, ThreadCommentsQueryVariables>({
    query: ThreadCommentsDocument,
    variables: { threadId },
    data: {
      ...cache,
      thread: {
        ...cache.thread,
        comments: {
          ...cache.thread.comments,
          edges: newEdges
        }
      }
    }
  });
};
