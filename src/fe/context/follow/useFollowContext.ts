// import * as myFollowedCollectionsCache from 'fe/collection/myFollowed/cache';
import { useCallOrNotifyMustLogin } from 'ctrl/lib/notifyMustLogin';
import * as userFollowedCollectionsCache from 'fe/collection/user/cache';
import * as myFollowedCommunitiesCache from 'fe/community/myFollowed/cache';
import * as userFollowedCommunitiesCache from 'fe/community/user/cache';
import * as collectionFollowersCache from 'fe/user/followers/collection/cache';
import * as communityFollowersCache from 'fe/user/followers/community/cache';
import * as userPageCache from 'fe/user/cache';
import { mnCtx } from 'fe/lib/graphql/ctx';
import { isOptimisticId } from 'fe/lib/helpers/mutations';
import * as GQL from 'fe/mutation/follow/useMutateFollow.generated';
import { useMe } from 'fe/session/useMe';
import Maybe from 'graphql/tsutils/Maybe';
import { Collection, Community, Follow, User } from 'graphql/types.generated';
import { useMemo } from 'react';
import { optimisticFollow } from './optimistics/follow';
import { optimisticUnfollow } from './optimistics/unfollow';

type Context = Collection | Community | User; //| Thread;

export type UseFollowContext = Maybe<
  Pick<Context, 'id' | 'followerCount' | '__typename' | 'name'> & {
    myFollow: Maybe<Pick<Follow, 'id'>>;
  }
>;

export const useFollowContext = (ctx: UseFollowContext) => {
  const { me } = useMe();
  const [followMut, followMutStatus] = GQL.useFollowMutation();
  const [unfollowMut, unfollowMutStatus] = GQL.useUnfollowMutation();
  const mutating = followMutStatus.loading || unfollowMutStatus.loading;
  const toggleFollow = useCallOrNotifyMustLogin(async () => {
    const myId = me?.user.id;
    if (!myId) {
      return;
    }
    const { id, followerCount, myFollow, __typename, name } = ctx || {};
    if (!id || mutating) {
      return;
    }
    if (!myFollow) {
      const context = mnCtx({
        ctx:
          __typename === 'Collection'
            ? `Following Collection ${name}`
            : __typename === 'User'
            ? `Following User ${name}`
            : __typename === 'Community'
            ? `Joined Community ${name}`
            : void 0
      });
      return followMut({
        variables: {
          contextId: id
        },
        context,
        optimisticResponse: optimisticFollow(__typename, id, followerCount || 0),
        update: (proxy, result) => {
          const context = result.data?.createFollow?.context;
          const myFollow = context?.myFollow;
          if (!(context && myFollow && me?.user)) {
            return;
          } else if (context.__typename === 'Community') {
            const communityFollow = {
              ...myFollow,
              context
            };
            myFollowedCommunitiesCache.addOne(proxy, {
              communityFollow
            });
            userFollowedCommunitiesCache.addOrSubstituteOne(proxy, {
              communityFollow,
              userId: myId
            });
            communityFollowersCache.addOne(proxy, {
              communityId: context.id,
              follower: {
                ...communityFollow,
                creator: { ...me.user, userId: me.user.id, userName: me.user.name }
              }
            });
          } else if (context.__typename === 'Collection') {
            const collectionFollow = {
              ...myFollow,
              context
            };
            userFollowedCollectionsCache.addOrSubstituteOne(proxy, {
              collectionFollow,
              userId: myId
            });
            collectionFollowersCache.addOne(proxy, {
              collectionId: context.id,
              follower: {
                ...collectionFollow,
                creator: { ...me.user, userId: me.user.id, userName: me.user.name }
              }
            });
          }

          if (
            context.__typename === 'Community' ||
            context.__typename === 'Collection' ||
            context.__typename === 'User'
          ) {
            userPageCache.addFollowsAmount(proxy, {
              userId: myId,
              on: context.__typename,
              amount: 1
            });
          }
        }
      });
    } else {
      const context = mnCtx({
        ctx:
          __typename === 'Collection'
            ? `Unfollowing Collection ${name}`
            : __typename === 'User'
            ? `Unfollowing User ${name}`
            : __typename === 'Community'
            ? `Leaving Community ${name}`
            : void 0
      });

      return isOptimisticId(myFollow.id)
        ? undefined
        : unfollowMut({
            variables: {
              contextId: myFollow.id
            },
            context,
            optimisticResponse: optimisticUnfollow(__typename, id, followerCount || 1),
            update: (proxy, result) => {
              if (result.data?.delete?.__typename !== 'Follow') {
                return;
              }
              const context = result.data.delete.context;
              const contextId = context.__typename === 'User' ? context.userId : context.id;
              const __typename = context.__typename;
              if (__typename === 'Community') {
                myFollowedCommunitiesCache.removeOne(proxy, {
                  communityFollowId: myFollow.id
                });
                // userFollowedCommunitiesCache.removeOne(proxy, {
                //   communityFollowId: myFollow.id,
                //   userId: myId
                // });
                communityFollowersCache.removeOne(proxy, {
                  communityId: contextId,
                  followId: myFollow.id
                });
              } else if (__typename === 'Collection') {
                // userFollowedCollectionsCache.removeOne(proxy, {
                //   collectionFollowId: myFollow.id,
                //   userId: myId
                // });
                collectionFollowersCache.removeOne(proxy, {
                  collectionId: contextId,
                  followId: myFollow.id
                });
              }

              if (
                __typename === 'Community' ||
                __typename === 'Collection' ||
                __typename === 'User'
              ) {
                userPageCache.addFollowsAmount(proxy, {
                  userId: myId,
                  on: __typename,
                  amount: -1
                });
              }
            }
          });
    }
  }, [ctx, mutating]);

  return useMemo(
    () => ({
      toggleFollow
    }),
    [toggleFollow]
  );
};
