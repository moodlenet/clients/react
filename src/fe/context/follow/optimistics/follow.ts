import { FollowMutation } from 'fe/mutation/follow/useMutateFollow.generated';
import { OPTIMISTIC_ID_STRING } from 'fe/lib/helpers/mutations';

export const optimisticFollow = (
  __typename: any,
  id: string,
  followerCount: number
): FollowMutation => ({
  __typename: 'RootMutationType',
  createFollow: {
    __typename: 'Follow',
    context: {
      __typename,
      id,
      myFollow: { __typename: 'Follow', id: OPTIMISTIC_ID_STRING },
      followerCount: followerCount + 1
    }
  }
});
