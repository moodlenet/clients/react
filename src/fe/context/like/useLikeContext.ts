import { useCallOrNotifyMustLogin } from 'ctrl/lib/notifyMustLogin';
import { isOptimisticId } from 'fe/lib/helpers/mutations';
import * as GQL from 'fe/mutation/like/useMutateLike.generated';
import Maybe from 'graphql/tsutils/Maybe';
import {
  // Community,
  // Collection,
  // User,
  Comment,
  Resource,
  ActivityVerb
} from 'graphql/types.generated';
import { useMemo } from 'react';

// profile-timeline (my outbox)	add on top *
import * as profileActivityOutboxTimelineCache from 'fe/activities/outbox/user/cache';

// profile-starred	add on top
import * as userLikesCache from 'fe/likes/user/cache';

import { fakeActivityPreviewFragment } from 'fe/activities/helpers/fakeActivity';
import { useMe } from 'fe/session/useMe';

type Typename = Exclude<
  // | Collection['__typename']
  | Comment['__typename']
  // | Community['__typename']
  // | User['__typename']
  | Resource['__typename'],
  null | undefined
>;

export const useLikeContext = (
  contextId: Maybe<string>,
  myLike: Maybe<{ id: string }>,
  likerCount: Maybe<number>,
  __typename: Typename
) => {
  const { me } = useMe();
  const [likeMut, likeMutStatus] = GQL.useLikeMutation();
  const [unlikeMut, unlikeMutStatus] = GQL.useUnlikeMutation();
  const mutating = likeMutStatus.loading || unlikeMutStatus.loading;
  const toggleLike = useCallOrNotifyMustLogin(async () => {
    if (!contextId || mutating) {
      return;
    }
    if (!myLike) {
      return likeMut({
        variables: {
          contextId
        },
        update: (proxy, result) => {
          if (!(result.data?.createLike && me?.user)) {
            return;
          }
          const like = result.data?.createLike;
          const activity = fakeActivityPreviewFragment({
            context: like,
            user: me.user,
            verb: ActivityVerb.Created
          });
          profileActivityOutboxTimelineCache.addOne(proxy, { activity, userId: me.user.id });
          userLikesCache.addOrSubstituteOne(proxy, { like, userId: me.user.id });
        }
      });
    } else {
      return isOptimisticId(myLike.id)
        ? undefined
        : unlikeMut({
            variables: {
              contextId: myLike.id
            },
            update: (proxy, result) => {
              if (!(result.data?.delete && me?.user)) {
                return;
              }
              profileActivityOutboxTimelineCache.removeOne(proxy, {
                userId: me.user.id,
                contextId: myLike.id
              });
              // userLikesCache.removeOne(proxy, {
              //   userId: me.user.id,
              //   likeId: myLike.id
              // });
            }
          });
    }
  }, [contextId, myLike, mutating, __typename, likerCount]);

  return useMemo(
    () => ({
      toggleLike
    }),
    [toggleLike]
  );
};
