import { UnflagMutation } from 'fe/mutation/flag/useMutateFlag.generated';

export const optimisticUnflag = (__typename: any, id: string): UnflagMutation => ({
  __typename: 'RootMutationType',
  delete: {
    __typename: 'Flag',
    context: {
      __typename,
      id,
      myFlag: null
    }
  }
});
