import { useCallOrNotifyMustLogin } from 'ctrl/lib/notifyMustLogin';
import { AllFlagsQueryRefetch } from 'fe/flags/all/useAllFlags.generated';
import { isOptimisticId } from 'fe/lib/helpers/mutations';
import * as GQL from 'fe/mutation/flag/useMutateFlag.generated';
import Maybe from 'graphql/tsutils/Maybe';
import { Collection, Comment, Community, Flag, Resource, User } from 'graphql/types.generated';
import { useMemo } from 'react';
import { optimisticFlag } from './optimistics/flag';
import { optimisticUnflag } from './optimistics/unflag';

type Context = Collection | Comment | Community | User | Resource;

export type FollowContext = Pick<Context, 'id' | '__typename'> & {
  myFlag: Maybe<Pick<Flag, 'id'>>;
};

export type UseFollowContext = Maybe<FollowContext>;

export const useFlagContext = (ctx: Maybe<UseFollowContext>) => {
  const [flagMut, flagMutStatus] = GQL.useFlagMutation();
  const [unflagMut, unflagMutStatus] = GQL.useUnflagMutation();
  const mutating = flagMutStatus.loading || unflagMutStatus.loading;
  const flag = useCallOrNotifyMustLogin(
    async (message: string) => {
      if (!ctx || ctx.myFlag || mutating) {
        return;
      }
      return flagMut({
        variables: {
          contextId: ctx.id,
          message
        },
        optimisticResponse: optimisticFlag(ctx.__typename, ctx.id),
        refetchQueries: [AllFlagsQueryRefetch({})]
      });
    },
    [ctx, mutating]
  );

  const unflag = useCallOrNotifyMustLogin(async () => {
    if (!ctx || !ctx.myFlag || mutating) {
      return;
    }
    return isOptimisticId(ctx.myFlag.id)
      ? undefined
      : unflagMut({
          variables: {
            contextId: ctx.myFlag.id
          },
          optimisticResponse: optimisticUnflag(ctx.__typename, ctx.id)
        });
  }, [ctx, mutating]);

  return useMemo(
    () => ({
      flag,
      unflag
    }),
    [flag, unflag]
  );
};
