import { useCallOrNotifyMustLogin } from 'ctrl/lib/notifyMustLogin';
import { fakeActivityPreviewFragment } from 'fe/activities/helpers/fakeActivity';
// my-moodlenet-timeline (my inbox)	add on top *
import * as myActivityInboxTimelineCache from 'fe/activities/inbox/my/cache';
// instance-timeline	add on top *
import * as instanceActivityOutboxTimelineCache from 'fe/activities/outbox/instance/cache';
// profile-timeline (my outbox)	add on top *
import * as userActivityOutboxTimelineCache from 'fe/activities/outbox/user/cache';
import { useCreateThreadMutation } from 'fe/mutation/createThread/useCreateThread.generated';
import { useMe } from 'fe/session/useMe';
// community's discussions list	add on top
import * as communityDiscussionsCache from 'fe/thread/community/cache';
import Maybe from 'graphql/tsutils/Maybe';
import { ActivityVerb, Community } from 'graphql/types.generated';
import { useMemo } from 'react';

export type ContextTypes = Community;
export type Context = Pick<ContextTypes, '__typename' | 'id'>;

export const useCreateThreadContext = (
  contextId: Maybe<Context['id']>,
  __typename: Maybe<Context['__typename']>
) => {
  const [createThreadMut, createThreadMutStatus] = useCreateThreadMutation();
  const { me } = useMe();
  const mutating = createThreadMutStatus.loading;
  const createThread = useCallOrNotifyMustLogin(
    async (content: string) => {
      if (!contextId || mutating) {
        return;
      }
      return createThreadMut({
        variables: {
          contextId,
          comment: { content }
        },
        update: (proxy, result) => {
          if (!(result.data?.createThread && me?.user)) {
            return;
          }
          const comment = result.data.createThread;
          const activity = fakeActivityPreviewFragment({
            context: comment,
            user: me.user,
            verb: ActivityVerb.Created
          });
          instanceActivityOutboxTimelineCache.addOne(proxy, { activity });
          userActivityOutboxTimelineCache.addOne(proxy, { activity, userId: me.user.id });
          myActivityInboxTimelineCache.addOne(proxy, { activity });
          if (comment.thread?.context?.__typename === 'Community') {
            const communityId = comment.thread.context.id;
            communityDiscussionsCache.addOne(proxy, { communityId, thread: comment.thread });
          }
        }
      }).then(res => res.data?.createThread?.thread?.id);
    },
    [contextId, __typename, mutating]
  );

  return useMemo(
    () => ({
      createThread
    }),
    [createThread]
  );
};
