import { CollectionPreviewFragment } from 'ctrl/modules/previews/collection/CollectionPreview.generated';
import { CommentPreviewFragment } from 'ctrl/modules/previews/comment/CommentPreview.generated';
import { CommunityPreviewFragment } from 'ctrl/modules/previews/community/CommunityPreview.generated';
import { FlagPreviewFragment } from 'ctrl/modules/previews/flag/FlagPreview.generated';
import { FollowPreviewFragment } from 'ctrl/modules/previews/follow/FollowPreview.generated';
import { LikePreviewFragment } from 'ctrl/modules/previews/like/LikePreview.generated';
import { ResourcePreviewFragment } from 'ctrl/modules/previews/resource/ResourcePreview.generated';
import { UserPreviewFragment } from 'ctrl/modules/previews/user/UserPreview.generated';

export type ActorPreviewFragment =
  | CommentPreviewFragment
  | ResourcePreviewFragment
  | CollectionPreviewFragment
  | CommunityPreviewFragment
  | UserPreviewFragment;

export type ActivityContextPreviewFragment =
  | ActorPreviewFragment
  | FlagPreviewFragment
  | LikePreviewFragment
  | FollowPreviewFragment;
