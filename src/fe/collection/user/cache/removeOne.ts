import { DataProxy } from 'apollo-cache';
import {
  UserFollowedCollectionsDocument,
  UserFollowedCollectionsQuery,
  UserFollowedCollectionsQueryVariables,
  UserCollectionFollowFragment
} from '../useUserFollowedCollections.generated';
import { User } from 'graphql/types.generated';

type Data = {
  collectionFollowId: UserCollectionFollowFragment['id'];
  userId: User['id'];
};

export const removeOne = (proxy: DataProxy, { collectionFollowId, userId }: Data) => {
  const cache = proxy.readQuery<
    UserFollowedCollectionsQuery,
    UserFollowedCollectionsQueryVariables
  >({
    query: UserFollowedCollectionsDocument,
    variables: { userId }
  });
  if (!cache?.user?.collectionFollows?.edges) {
    return;
  }
  const edges = cache.user.collectionFollows.edges.filter(
    follow => follow.id !== collectionFollowId
  );
  proxy.writeQuery<UserFollowedCollectionsQuery, UserFollowedCollectionsQueryVariables>({
    query: UserFollowedCollectionsDocument,
    variables: { userId },
    data: {
      ...cache,
      user: {
        ...cache.user,
        collectionFollows: {
          ...cache.user.collectionFollows,
          edges
        }
      }
    }
  });
};
