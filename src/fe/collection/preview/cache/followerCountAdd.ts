import { DataProxy } from 'apollo-cache';
import {
  CollectionPreviewDocument,
  CollectionPreviewQueryVariables,
  CollectionPreviewQuery
} from '../useCollectionPreview.generated';
import { Collection } from 'graphql/types.generated';
import { CollectionPreviewFragment } from 'ctrl/modules/previews/collection/CollectionPreview.generated';

export type Data = {
  collectionId: Collection['id'];
  amount: number;
};
export const followerCountAdd = (proxy: DataProxy, { amount, collectionId }: Data) => {
  const cache = proxy.readQuery<CollectionPreviewQuery, CollectionPreviewQueryVariables>({
    query: CollectionPreviewDocument,
    variables: { collectionId }
  });
  if (!(cache?.collection && amount)) {
    return;
  }
  const newAmount = (cache.collection.followerCount || 0) + Math.ceil(amount);

  const newCollection: CollectionPreviewFragment = {
    ...cache.collection,
    followerCount: newAmount
  };

  proxy.writeQuery<CollectionPreviewQuery, CollectionPreviewQueryVariables>({
    query: CollectionPreviewDocument,
    data: { ...cache, collection: newCollection },
    variables: { collectionId }
  });
};
