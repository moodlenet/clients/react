import { DataProxy } from 'apollo-cache';
import {
  AllCollectionsDocument,
  AllCollectionsQuery,
  AllCollectionsQueryVariables
} from '../useAllCollections.generated';
import { CollectionPreviewFragment } from 'ctrl/modules/previews/collection/CollectionPreview.generated';

type Data = {
  collection: CollectionPreviewFragment;
};

export const addOne = (proxy: DataProxy, { collection }: Data) => {
  const cache = proxy.readQuery<AllCollectionsQuery, AllCollectionsQueryVariables>({
    query: AllCollectionsDocument
  });
  if (!cache?.collections.edges) {
    return;
  }
  const oldEdges = cache.collections.edges;
  type Edges = typeof oldEdges;
  const newEdges: Edges = [collection, ...oldEdges];
  proxy.writeQuery<AllCollectionsQuery, AllCollectionsQueryVariables>({
    query: AllCollectionsDocument,
    data: {
      ...cache,
      collections: {
        ...cache.collections,
        edges: newEdges
      }
    }
  });
};
