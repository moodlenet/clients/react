import { DataProxy } from 'apollo-cache';
import {
  CommunityCollectionsDocument,
  CommunityCollectionsQuery,
  CommunityCollectionsQueryVariables,
  CommunityCollectionFragment
} from '../useCommunityCollections.generated';
import { CollectionPreviewFragment } from 'ctrl/modules/previews/collection/CollectionPreview.generated';
import { Community } from 'graphql/types.generated';

type Data = {
  collection: CollectionPreviewFragment;
  communityId: Community['id'];
};

export const addOne = (proxy: DataProxy, { collection, communityId }: Data) => {
  const cache = proxy.readQuery<CommunityCollectionsQuery, CommunityCollectionsQueryVariables>({
    query: CommunityCollectionsDocument,
    variables: { communityId }
  });
  if (!cache?.community?.collections?.edges) {
    return;
  }
  const newEdges: CommunityCollectionFragment[] = [
    collection,
    ...cache.community.collections.edges
  ];
  proxy.writeQuery<CommunityCollectionsQuery, CommunityCollectionsQueryVariables>({
    query: CommunityCollectionsDocument,
    variables: { communityId },
    data: {
      ...cache,
      community: {
        ...cache.community,
        collections: {
          ...cache.community.collections,
          edges: newEdges
        }
      }
    }
  });
};
