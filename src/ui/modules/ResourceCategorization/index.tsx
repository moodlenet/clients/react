import { Trans } from '@lingui/macro';
import React from 'react';
import Select from 'react-select';
import { ValueType } from 'react-select/src/types';
import { FormBag } from 'ui/@types/types';
import Alert from 'ui/elements/Alert';
import styled from 'ui/themes/styled';
import { clearFix } from 'polished';

export interface OptionGroup {
  options: Option[];
  label: string;
}

export interface Option {
  value: string;
  label: string;
}

export const getFirst = (option: ValueType<Option>): Option | null | undefined =>
  Array.isArray(option) ? option[0] : option;

export type Field = 'subject' | 'grade' | 'language' | 'type' | 'format';
export interface ResourceCategorization {
  formik: FormBag<ResourceCategorizationValues>;
  subjects: Option[];
  grades: Option[];
  languages: Option[];
  types: Option[];
  licenses: OptionGroup[];
  formats: OptionGroup[];
  searchInputChange(field: Field): (search: string) => unknown;
}

export interface ResourceCategorizationValues {
  subject: Option;
  grade: Option;
  language: Option;
  type: Option;
  license: Option;
  format: Option;
}

export const ResourceCategorization: React.FC<ResourceCategorization> = ({
  formik,
  grades,
  languages,
  subjects,
  types,
  licenses,
  formats,
  searchInputChange
}) => {
  return (
    <>
      <FormRow>
        <label>
          <Trans>Subject</Trans>
        </label>
        <FormField>
          <Select
            options={subjects}
            onChange={v => formik.setFieldValue('subject', getFirst(v))}
            value={formik.values.subject}
            isSearchable
            onInputChange={searchInputChange('subject')}
          />
          {formik.errors.subject && <Alert variant="negative">{formik.errors.subject}</Alert>}
        </FormField>
      </FormRow>
      <FormRow>
        <label>
          <Trans>Level/Grade</Trans>
        </label>
        <FormField>
          <Select
            options={grades}
            onChange={v => formik.setFieldValue('grade', getFirst(v))}
            value={formik.values.grade}
            isSearchable
            onInputChange={searchInputChange('grade')}
          />
          {formik.errors.grade && <Alert variant="negative">{formik.errors.grade}</Alert>}
        </FormField>
      </FormRow>
      <FormRow>
        <label>
          <Trans>Language</Trans>
        </label>
        <FormField>
          <Select
            options={languages}
            onChange={v => formik.setFieldValue('language', getFirst(v))}
            value={formik.values.language}
            isSearchable
            onInputChange={searchInputChange('language')}
          />
          {formik.errors.language && <Alert variant="negative">{formik.errors.language}</Alert>}
        </FormField>
      </FormRow>
      <FormRow>
        <label>
          <Trans>Type</Trans>
        </label>
        <FormField>
          <Select
            options={types}
            onChange={v => formik.setFieldValue('type', getFirst(v))}
            value={formik.values.type}
            isSearchable
            onInputChange={searchInputChange('type')}
          />
          {formik.errors.type && <Alert variant="negative">{formik.errors.type}</Alert>}
        </FormField>
      </FormRow>
      <FormRow>
        <label>
          <Trans>Format</Trans>
        </label>
        <FormField>
          <Select
            options={formats}
            onChange={v => formik.setFieldValue('format', getFirst(v))}
            value={formik.values.format}
            isSearchable
            onInputChange={searchInputChange('format')}
          />
          {formik.errors.format && <Alert variant="negative">{formik.errors.format}</Alert>}
        </FormField>
      </FormRow>
      <FormRow>
        <label>
          <Trans>License</Trans>
        </label>
        <FormField>
          <Select
            options={licenses}
            onChange={v => formik.setFieldValue('license', getFirst(v))}
            value={formik.values.license}
          />
          {formik.errors.license && <Alert variant="negative">{formik.errors.license}</Alert>}
        </FormField>
      </FormRow>
    </>
  );
};

export const FormField = styled.div`
  flex: 1;
  ${clearFix()};
  input {
    height: 40px;
    background: white;
    border-radius: 2px;
    border: ${props => props.theme.colors.border};
  }
  textarea {
    line-height: 20px;
    background: white;
    border-radius: 2px;
    border: ${props => props.theme.colors.border};
    font-family: ${props => props.theme.fontFamily};
  }
`;
export const FormRow = styled.div<{ big?: boolean }>`
  ${clearFix()};
  border-bottom: 1px solid ${props => props.theme.colors.lighter};
  height: ${props => (props.big ? '180px' : 'auto')};
  padding: 20px;
  & textarea {
    height: 120px;
  }
  & label {
    line-height: 40px;
    width: 100%;
  }

  display: block;
`;
