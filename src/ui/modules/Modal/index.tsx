import { clearFix } from 'polished';
import React, { useCallback, useEffect } from 'react';
import ReactDOM from 'react-dom';
// import Icons from 'ui/atoms/icons.tsx'
import styled from 'ui/themes/styled';
import { Box } from 'rebass/styled-components';
import { X } from 'react-feather';
import media from 'styled-media-query';

const ModalContainer = styled.div`
  background-color: rgba(51, 60, 69, 0.95);
  position: fixed;
  overflow-y: auto;
  overflow-x: hidden;
  top: 0;
  left: 0;
  height: 100%;
  width: 100%;
  z-index: 9999999900;
`;

export const AlertWrapper = styled.div`
  margin-top: 4px;
  z-index: 99999;
  position: relative;
`;

const Dialog = styled.div`
  max-width: 700px;
  box-shadow: 0 2px 8px 3px rgba(0, 0, 0, 0.3);
  background-color: #ffffff;
  padding: 0;

  margin: 40px auto;
  width: auto;

  position: relative;
  border-radius: 3px;
  outline: none;

  ${media.lessThan('medium')`
    margin: 40px 25px;
    left: 8px;
    right: 8px;
  `};
`;

const Action = styled.div`
  ${clearFix()};
  position: relative;
`;

const Close = styled(Box)`
  position: absolute;
  right: -10px;
  top: -10px;
  cursor: pointer;
  background: ${props => props.theme.colors.primary};
  width: 24px;
  height: 24px;
  border-radius: 40px;
  display: flex;
  justify-content: space-around;
  align-items: center;
  z-index: 999999999;
`;

const Content = styled.div`
  ${clearFix()};
  font-family: ${props => props.theme.fontFamily};
`;

export const Container = styled.div`
  font-family: ${props => props.theme.fontFamily};
`;
export const Actions = styled(Box)`
  ${clearFix()};
  height: 60px;
  padding-top: 10px;
  padding-right: 10px;
  & button {
    float: right;
  }
`;

export const CounterChars = styled.div`
  float: right;
  font-size: 11px;
  text-transform: uppercase;
  background: #d0d9db;
  padding: 2px 10px;
  font-weight: 600;
  margin-top: 4px;
  color: #32302e;
  letter-spacing: 1px;
`;

export const ContainerForm = styled.div`
  flex: 1;
  ${clearFix()};
  input {
    height: 40px;
    background: white;
    border-radius: 2px;
    border: ${props => props.theme.colors.border};
  }
  textarea {
    line-height: 20px;
    background: white;
    border-radius: 2px;
    border: ${props => props.theme.colors.border};
    font-family: ${props => props.theme.fontFamily};
  }
`;

export const Header = styled.div`
  padding: 0 16px;
  padding-top: 16px;
  // border-bottom: 1px solid ${props => props.theme.colors.lighter};
  & h5 {
    text-align: center !important;
    margin: 0 !important;
    font-size: 18px;
  }
`;

export const Row = styled.div<{ big?: boolean }>`
  ${clearFix()};
  border-bottom: 1px solid ${props => props.theme.colors.lighter};
  height: ${props => (props.big ? '180px' : 'auto')};
  display: flex;
  padding: 20px;
  & textarea {
    height: 120px;
  }
  & label {
    width: 200px;
    line-height: 40px;
    ${media.lessThan('medium')`
    width: 100%;
  `};
  }

  ${media.lessThan('medium')`
    display: block;

  `};
`;

class Portal extends React.Component {
  static el = (() => {
    const _el = document.createElement('div');
    _el.setAttribute('id', 'modalPortal');
    _el.style.display = 'none';
    document.body.prepend(_el);
    return _el;
  })();
  componentDidMount() {
    Portal.el.style.display = 'block';
  }

  componentWillUnmount() {
    Portal.el.style.display = 'none';
  }

  render() {
    return ReactDOM.createPortal(this.props.children, Portal.el);
  }
}

interface Props {
  closeModal: () => void;
}

const stopPropagation = (event: React.MouseEvent) => event.stopPropagation();
const Modal: React.FC<Props> = ({ closeModal, children }) => {
  const handleCloseModal = useCallback(
    (event: React.MouseEvent) => {
      event.stopPropagation();
      closeModal();
    },
    [closeModal]
  );
  useEffect(() => {
    const handleEvent = ({ keyCode }: KeyboardEvent) => {
      if (keyCode === 27) {
        closeModal();
      }
    };
    document.addEventListener('keyup', handleEvent);
    return () => document.removeEventListener('keyup', handleEvent);
  }, [closeModal]);
  return (
    <Portal>
      <ModalContainer onClick={handleCloseModal}>
        {/* <Background onClick={handleCloseModal} /> */}
        <Dialog onClick={stopPropagation}>
          <Action>
            <Close onClick={handleCloseModal}>
              <X color="#fff" size={16} />
            </Close>
          </Action>
          <Content>{children}</Content>
        </Dialog>
      </ModalContainer>
    </Portal>
  );
};

export default Modal;
