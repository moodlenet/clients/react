import React from 'react';
import { File, Image } from 'react-feather';
import { Box, Flex } from 'rebass/styled-components';
import Alert from 'ui/elements/Alert';
import styled from 'ui/themes/styled';

export interface DropzoneArea {
  isActive?: boolean;
  isImage?: undefined | boolean;
  currentFilename?: undefined | string;
  currentImageUrl?: undefined | string;
  error?: undefined | string;
  mainDivProps?: React.HTMLAttributes<HTMLDivElement>;
}

export const DropzoneArea: React.FC<DropzoneArea> = ({
  isActive = false,
  currentFilename,
  currentImageUrl,
  isImage,
  error,
  mainDivProps
}) => {
  const WithIcon = !isImage ? File : Image;
  return (
    <Box {...mainDivProps} sx={{ height: '100%' }}>
      <InfoContainer className={isActive ? 'active' : 'none'}>
        {!currentFilename && !currentImageUrl ? (
          <Thumb>
            <WrapperIcon>
              <WithIcon size={30} strokeWidth={1} color={'rgba(250,250,250, .5)'} />
            </WrapperIcon>
          </Thumb>
        ) : isImage ? (
          <Thumb className="thumb">
            <WrapperIcon>
              <Image size={30} strokeWidth={1} color={'rgba(250,250,250, .5)'} />
            </WrapperIcon>
            <Img
              style={{
                backgroundImage: `url("${currentImageUrl}")`
              }}
            />
          </Thumb>
        ) : (
          <FileThumb>
            <File size={20} />
            <FileName>{currentFilename}</FileName>
          </FileThumb>
        )}
        {error && <Alert variant={'negative'}>{error}</Alert>}
      </InfoContainer>
    </Box>
  );
};

const InfoContainer = styled.div`
  background: ${props => props.theme.colors.lighter};
  border-radius: 4px;
  text-align: center;
  cursor: pointer;
  box-sizing: border-box;
  height: inherit;
  margin: 0px;
  &.active {
    border: 1px dashed ${props => props.theme.colors.primary};
  }
  .;
`;

const FileName = styled.p`
  margin-bottom: 0px;
  margin-top: 5px;
  font-weight: bold;
  text-align: center;
  font-style: italic;
`;

const WrapperIcon = styled(Flex)`
  width: 40px;
  height: 40px;
  align-items: center;
  border-radius: 100px;
  position: absolute;
  left: 50%;
  margin-left: -20px;
  top: 50%;
  margin-top: -20px;
  z-index: 9;
`;

// const WrapperFile = styled.div`
//   padding: 20px 10px;
//   border-radius: 4px;
// `;

const FileThumb = styled.div`
  padding: 20px 10px;
  border-radius: 4px;
  background: rgba(0, 0, 0, 0.3);

  &:hover {
    background: rgba(0, 0, 0, 0.2);
  }
`;

const Thumb = styled.div`
  width: 100%;
  box-sizing: border-box;
  position: relative;
  height: 120px;
  &:after {
    position: absolute;
    content: '';
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;
    border-radius: 4px;
    display: block;
    background: rgba(0, 0, 0, 0.3);
  }
  &:hover {
    &:after {
      background: rgba(0, 0, 0, 0.1);
    }
  }
  svg {
    width: 40px;
  }
`;

const Img = styled(Box)`
    display: block;
    border-radius: 4px;
    height: inherit;
    background-size: cover;
    background-position: center;
}
`;
