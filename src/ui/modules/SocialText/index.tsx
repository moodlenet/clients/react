import { Trans } from '@lingui/macro';
import { Textarea } from '@rebass/forms';
import React from 'react';
import { Flex } from 'rebass/styled-components';
// import { dropEmoji } from '../../lib/emoji';
// import EmojiPicker from 'emoji-picker-react';
// import OutsideClickHandler from 'react-outside-click-handler';
import Button from 'ui/elements/Button';
import styled from 'ui/themes/styled';
// const PickerWrap = styled.div`
//   position: absolute;
//   right: 10px;
//   top: 45px;
//   z-index: 999999999999999999;
// `;
const Wrapper = styled(Flex)`
  width: 100%;
  position: relative;
  height: 200px;
  flex-direction: column;
`;
const SocialTextDiv = styled(Flex)`
  position: relative;
  width: 100%;
  align-items: center;
  border: ${props => props.theme.colors.border};
  border-radius: 4px;
  margin-bottom: 8px;
  background: white;
  flex: 1;
`;
// const EmojiPickerTrigger = styled(Box)`
//   cursor: pointer;
//   &:hover {
//     svg {
//       stroke: ${props => props.theme.colors.primary}
//     }
//   }
// `;

const SocialTextArea = styled(Textarea)`
  height: 100%;
  resize: none;
  background: ${props => props.theme.colors.appInverse};
  flex: 1;
  border: 0 !important;
  font-size: 16px !important;
  &:focus {
    outline: none;
  }
  font-family: 'Open Sans', sans-serif !important;
`;

const SocialActions = styled(Flex)`
  align-self: flex-end;
`;

export interface SocialText {
  textAreaProps?: React.TextareaHTMLAttributes<HTMLTextAreaElement>;
  submit?: undefined | (() => unknown);
}
export const SocialText: React.FC<SocialText> = ({ textAreaProps, submit }) => {
  return (
    <Wrapper>
      <SocialTextDiv>
        <SocialTextArea {...textAreaProps} />
        {/* {isEmojiOpen && (
          <OutsideClickHandler onOutsideClick={toggleEmoji}>
            <PickerWrap>
              <EmojiPicker preload onEmojiClick={addEmoji} />
            </PickerWrap>
          </OutsideClickHandler>
        )} */}
      </SocialTextDiv>
      <SocialActions>
        {/* <EmojiPickerTrigger onClick={toggleEmoji}>
            <Smile color={'rgba(0,0,0,.4)'} size="24" />
          </EmojiPickerTrigger> */}
        <Button
          variant="primary"
          sx={{
            textTransform: 'capitalize'
          }}
          style={{
            cursor: submit ? 'pointer' : 'default'
          }}
          onClick={submit}
          disabled={!submit}
        >
          <Trans>Publish</Trans>
        </Button>
      </SocialActions>
    </Wrapper>
  );
};
export default SocialText;
