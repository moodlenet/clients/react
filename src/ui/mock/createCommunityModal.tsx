import React from 'react';
import { useFormik } from 'formik';
import { action } from '@storybook/addon-actions';
import {
  CreateCommunityFormValues,
  Props as CreateCommunityProps
} from 'ui/modules/CreateCommunityPanel';
import { DropzoneArea } from 'ui/modules/DropzoneArea';

export const useGetCreateCommunityModalProps = (): CreateCommunityProps => {
  const formik = useFormik<CreateCommunityFormValues>({
    initialValues: {
      name: '',
      summary: ''
    },
    onSubmit: () => {
      action('submit')();
      return new Promise((resolve, reject) => {
        setTimeout(resolve, 3000);
      });
    }
  });
  return {
    formik,
    cancel: action('cancel'),
    IconElement: <DropzoneArea isImage />
  };
};
