import React from 'react';
import { Props } from 'ui/modules/ShareLink';
import { useFormik } from 'formik';
import { action } from '@storybook/addon-actions';
import { ShareResourceFormValues } from 'ui/modules/ShareLink/fetched';
import { DropzoneArea } from 'ui/modules/DropzoneArea';

export const useGetShareLinkProps = (): Props => {
  return {
    CategorizationPanel: <></>,
    FetchLinkFormik: useFormik<{ fetchUrl: string }>({
      initialValues: {
        fetchUrl: ''
      },
      onSubmit: vals => {
        action('submitting...')();
        return new Promise(resolve =>
          setTimeout(() => {
            action('submitted...')();
            resolve();
          }, 2000)
        );
      }
    }),
    isFetched: true,
    cancelFetched: action('submitting...'),
    formik: useFormik<ShareResourceFormValues>({
      initialValues: {
        name: '',
        summary: ''
      },
      onSubmit: vals => {
        action('submitting...')();
        return new Promise(resolve =>
          setTimeout(() => {
            action('submitted...')();
            resolve();
          }, 2000)
        );
      }
    }),
    IconElement: <DropzoneArea isImage />
  };
};
