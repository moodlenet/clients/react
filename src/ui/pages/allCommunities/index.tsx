import * as React from 'react';
import { Header } from 'ui/modules/Header';
import { WrapperPanel } from 'ui/elements/Panel';
import { LoadMore } from 'ui/modules/Loadmore';
import { FormBag } from 'ui/@types/types';
import { Wrapper, WrapperCont, MainContainer, HomeBox, ObjectsList } from 'ui/elements/Layout';

export interface Props {
  CommunitiesBoxes: React.ReactElement;
  LoadMoreFormik: FormBag;
}

export const AllCommunities: React.FC<Props> = ({ CommunitiesBoxes, LoadMoreFormik }) => {
  return (
    <MainContainer>
      <HomeBox>
        <WrapperCont>
          <Wrapper>
            <Header name="All Communities" />
            <ObjectsList>{CommunitiesBoxes}</ObjectsList>
            <LoadMore LoadMoreFormik={LoadMoreFormik} />
          </Wrapper>
        </WrapperCont>
      </HomeBox>
      <WrapperPanel />
    </MainContainer>
  );
};
