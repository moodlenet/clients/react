import { ellipsis } from 'polished';
import * as React from 'react';
import { Link } from 'react-feather';
import { NavLink, Route, Switch } from 'react-router-dom';
import { Box, Flex, Text } from 'rebass/styled-components';
// import { Header } from 'ui/modules/Header';
import { FormBag } from 'ui/@types/types';
import {
  CollectionsWrapper,
  HomeBox,
  List,
  MainContainer,
  MenuList,
  ObjectsList,
  Wrapper,
  WrapperCont
} from 'ui/elements/Layout';
import { Nav, NavItem, Panel, PanelTitle, WrapperPanel } from 'ui/elements/Panel';
import { LoadMore } from 'ui/modules/Loadmore';
import styled from 'ui/themes/styled';
import { ReactElement } from 'react';

export interface Props {
  ActivityBoxes: ReactElement;
  LikesBoxes: ReactElement;
  HeroUserBox: ReactElement;
  CommunityBoxes: ReactElement;
  CollectionsBoxes: ReactElement;
  UserBoxes: ReactElement;
  totalCommunities: string;
  totalActivities: string;
  totalCollections: string;
  totalUsers: string;
  userLink: string;
  userName: string;
  loadMoreActivities: FormBag | null;
  loadMoreLikes: FormBag | null;
  loadMoreCommunities: FormBag | null;
  loadMoreCollections: FormBag | null;
  loadMoreFollowing: FormBag | null;
  tabPaths: {
    timeline: string;
    starred: string;
    communities: string;
    collections: string;
  };
}

export const User: React.FC<Props> = ({
  HeroUserBox,
  LikesBoxes,
  ActivityBoxes,
  CommunityBoxes,
  CollectionsBoxes,
  // UserBoxes,
  tabPaths,
  totalCommunities,
  userLink,
  totalCollections,
  // totalUsers,
  // userName,
  loadMoreActivities,
  loadMoreLikes,
  loadMoreCommunities,
  loadMoreCollections
  // loadMoreFollowing
}) => {
  return (
    <MainContainer>
      <HomeBox>
        <WrapperCont>
          <Wrapper>
            <Box mb={2}>
              {/* <Header name={userName} /> */}
              {HeroUserBox}
              <Menu
                tabPaths={tabPaths}
                totalCommunities={totalCommunities}
                totalCollections={totalCollections}
              />
            </Box>
            <Switch>
              <Route exact path={tabPaths.timeline}>
                <List>{ActivityBoxes}</List>
                {loadMoreActivities && <LoadMore LoadMoreFormik={loadMoreActivities} />}
              </Route>
              <Route exact path={tabPaths.starred}>
                <List>{LikesBoxes}</List>
                {loadMoreLikes && <LoadMore LoadMoreFormik={loadMoreLikes} />}
              </Route>
              <Route exact path={tabPaths.communities}>
                <ObjectsList>{CommunityBoxes}</ObjectsList>
                {loadMoreCommunities && <LoadMore LoadMoreFormik={loadMoreCommunities} />}
              </Route>
              <Route exact path={tabPaths.collections}>
                <ObjectsList>
                  <CollectionsWrapper>{CollectionsBoxes}</CollectionsWrapper>
                </ObjectsList>
                {loadMoreCollections && <LoadMore LoadMoreFormik={loadMoreCollections} />}
              </Route>
              {/* <Route path={`${basePath}/following`}>
                {UserBoxes}
                {loadMoreFollowing ? (
                  <LoadMore LoadMoreFormik={loadMoreFollowing} />
                ) : null}{' '} */}
              {/* FIX ME after add LoadMoreFormik */}
              {/* </Route> */}
            </Switch>
          </Wrapper>
        </WrapperCont>
      </HomeBox>
      <WrapperPanel>
        {userLink.length > 0 ? (
          <Panel>
            <PanelTitle fontSize={0} fontWeight={'bold'}>
              Relevant links
            </PanelTitle>
            <Nav>
              <NavItem fontSize={1}>
                <Flex>
                  <Link size={20} />{' '}
                  <a href={userLink} target="_blank" rel="noopener noreferrer">
                    <TextLink ml={2} flex={1}>
                      {userLink}
                    </TextLink>
                  </a>
                </Flex>
              </NavItem>
            </Nav>
          </Panel>
        ) : null}
      </WrapperPanel>
    </MainContainer>
  );
};

const Menu = ({
  tabPaths,
  totalCommunities,
  totalCollections
}: {
  tabPaths: Props['tabPaths'];
  totalCommunities: string;
  totalCollections: string;
}) => (
  <MenuList p={3} pt={3}>
    <NavLink exact to={tabPaths.timeline}>
      Recent activity
    </NavLink>
    <NavLink exact to={tabPaths.starred}>
      Starred
    </NavLink>
    <NavLink exact to={tabPaths.communities}>
      {totalCommunities} communities
    </NavLink>
    <NavLink exact to={tabPaths.collections}>
      {totalCollections} collections
    </NavLink>
  </MenuList>
);

const TextLink = styled(Text)`
  ${ellipsis('250px')};
  color: ${props => props.theme.colors.dark};
  &:hover {
    text-decoration: underline;
  }
`;
