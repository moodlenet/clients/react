import { Trans } from '@lingui/react';
import * as React from 'react';
import { ReactElement } from 'react';
import { Link } from 'react-router-dom';
import { Box, Flex, Text } from 'rebass/styled-components';
import { FormBag } from 'ui/@types/types';
import Avatar from 'ui/elements/Avatar';
import { HomeBox, MainContainer, ObjectsList, Wrapper, WrapperCont } from 'ui/elements/Layout';
import { WrapperPanel } from 'ui/elements/Panel';
import { LoadMore } from 'ui/modules/Loadmore';
import styled from 'ui/themes/styled';

export interface Props {
  MainThread: ReactElement;
  Comments: ReactElement;
  Context: ReactElement;
  communityLink: string;
  communityName: string;
  communityIcon: string;
  isCommunityContext: boolean;
  loadMoreComments: FormBag | null;
  SocialTextElement: ReactElement | null;
}

export const Thread: React.FC<Props> = ({
  MainThread,
  Comments,
  communityLink,
  communityName,
  communityIcon,
  Context,
  isCommunityContext,
  loadMoreComments,
  SocialTextElement
}) => {
  // console.log(Context);
  return (
    <MainContainer>
      <HomeBox mb={3}>
        <WrapperCont>
          <Wrapper>
            <Box mb={2}>
              {!isCommunityContext && <Box p={2}>{Context}</Box>}
              <MainThreadContainer>{MainThread}</MainThreadContainer>
            </Box>
            <ObjectsList className="replies">{Comments}</ObjectsList>
            {loadMoreComments && <LoadMore LoadMoreFormik={loadMoreComments} />}
            {SocialTextElement && <SocialWrapper my={3}>{SocialTextElement}</SocialWrapper>}
          </Wrapper>
        </WrapperCont>
      </HomeBox>
      <WrapperPanel>
        <TitleSection mb={2} variant="suptitle">
          <Trans>Community</Trans>
        </TitleSection>
        <HeaderWrapper link={communityLink} name={communityName} icon={communityIcon} />
      </WrapperPanel>
    </MainContainer>
  );
};

export const HeaderWrapper: React.FC<{ link: string; name: string; icon: string }> = ({
  link,
  name,
  icon
}) => {
  return (
    <>
      <Header>
        <Right>
          <Link to={link}>
            <LinkImg>
              <Avatar size="s" src={icon} />
            </LinkImg>
            <Text variant="suptitle">{name}</Text>
          </Link>
        </Right>
      </Header>
    </>
  );
};

const TitleSection = styled(Text)`
  text-transform: capitalize;
`;

const SocialWrapper = styled(Box)`
  // background: ${props => props.theme.colors.appInverse};
`;

const MainThreadContainer = styled(Box)`
  //  border-bottom: ${props => props.theme.colors.border};
`;

const LinkImg = styled(Box)`
  margin-right: 8px;
  .--rtl & {
    margin-right: 0px;
    margin-left: 8px;
  }
`;
const Right = styled(Flex)`
  align-items: center;
  a {
    display: flex;
    align-items: center;
  }
`;

const Header = styled(Flex)`
  border-bottom: ${props => props.theme.colors.border};
  height: 50px;
  align-items: center;
  justify-content: space-between;
  padding: 0 8px;
  cursor: pointer;
  background: ${props => props.theme.colors.appInverse};
  border-top-left-radius: 6px;
  border-top-right-radius: 6px;
  a {
    display: flex;
    flex: 1;
    text-decoration: none;
  }
`;
