import React, { FC, useMemo } from 'react';
import { CommunityPageTab, CommunityPage } from 'ctrl/pages/community/CommunityPage';
import { NotFoundCtrl } from 'ctrl/pages/not-found/NotFound';
import { RouteComponentProps, RouteProps } from 'react-router-dom';
import { WithSidebarTemplate } from 'ctrl/templates/WithSidebar/WithSidebar';
import { locationHelper } from './lib/helper';

interface CommunityPageRouter {
  communityId: string;
  tab?: string;
}
const CommunityPageRouter: FC<RouteComponentProps<CommunityPageRouter>> = ({ match }) => {
  const communityId = match.params.communityId;
  const maybeTabStr = match.params.tab;
  const tab =
    maybeTabStr === 'timeline'
      ? CommunityPageTab.Activities
      : maybeTabStr === 'members'
      ? CommunityPageTab.Members
      : maybeTabStr === 'discussions'
      ? CommunityPageTab.Discussions
      : !maybeTabStr
      ? CommunityPageTab.Collections
      : null;

  const props = useMemo<CommunityPage | null>(() => {
    return tab === null
      ? null
      : {
          communityId,
          tab,
          basePath: communityLocation.getPath({ communityId, tab: undefined }, undefined)
        };
  }, [tab, communityId]);

  if (props === null) {
    return <NotFoundCtrl />;
  }

  return (
    <WithSidebarTemplate>
      <CommunityPage {...props} />
    </WithSidebarTemplate>
  );
};

export const CommunityPageRoute: RouteProps = {
  exact: true,
  path: '/communities/:communityId/:tab(timeline|members|discussions)?',
  component: CommunityPageRouter
};

type Tab = undefined | 'timeline' | 'members' | 'discussions';
type Params = {
  communityId: string;
  tab: Tab;
};

export const communityLocation = locationHelper<Params, undefined>(CommunityPageRoute);
