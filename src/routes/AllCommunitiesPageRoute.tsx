import { AllCommunitiesPage } from 'ctrl/pages/all-communities/AllCommunities';
import React, { FC } from 'react';
import { RouteComponentProps, RouteProps } from 'react-router-dom';
import { WithSidebarTemplate } from 'ctrl/templates/WithSidebar/WithSidebar';

interface AllCommunitiesPageRouter {}
const AllCommunitiesPageRouter: FC<RouteComponentProps<AllCommunitiesPageRouter>> = () => {
  //const props: AllCommunitiesPage = {};
  return (
    <WithSidebarTemplate>
      <AllCommunitiesPage />
    </WithSidebarTemplate>
  );
};

export const AllCommunitiesPageRoute: RouteProps = {
  exact: true,
  path: '/communities',
  component: AllCommunitiesPageRouter
};
