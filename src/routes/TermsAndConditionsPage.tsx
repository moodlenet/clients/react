import React, { FC } from 'react';
import { RouteComponentProps, RouteProps } from 'react-router-dom';
import { GuestTemplate } from 'ctrl/templates/Guest/Guest';
import { TermsAndConditionsPageCtrl } from 'ctrl/pages/termsAndConditions/TermsAndConditionsPage';

interface TermsAndConditionsPageRouter {}
const TermsAndConditionsPageRouter: FC<RouteComponentProps<TermsAndConditionsPageRouter>> = () => {
  return (
    <GuestTemplate withoutHeader>
      <TermsAndConditionsPageCtrl />
    </GuestTemplate>
  );
};

export const TermsAndConditionsPageRoute: RouteProps = {
  exact: true,
  path: '/terms',
  component: TermsAndConditionsPageRouter
};
