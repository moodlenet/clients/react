import { WithSidebarTemplate } from 'ctrl/templates/WithSidebar/WithSidebar';
import { SearchPageCtrl } from 'ctrl/pages/search/Search';
import React, { FC } from 'react';
import { RouteComponentProps, RouteProps } from 'react-router-dom';
import { searchDisabled } from 'mn-constants';
import { NotFoundCtrl } from 'ctrl/pages/not-found/NotFound';

interface SearchPageRouter {}
const SearchPageRouter: FC<RouteComponentProps<SearchPageRouter>> = () => {
  return searchDisabled ? (
    <NotFoundCtrl />
  ) : (
    <WithSidebarTemplate>
      <SearchPageCtrl />
    </WithSidebarTemplate>
  );
};

export const SearchPageRoute: RouteProps = {
  exact: false,
  path: '/search',
  component: SearchPageRouter
};
