# MoodleNet User Agreement

## 1. Terms

By accessing or using this MoodleNet instance, you are agreeing to be bound by these terms, and all applicable laws and regulations. If you do not agree with any of these terms, you are prohibited from using or accessing this site. The software and materials contained in this website are protected by applicable copyright law and open source and commons licences as indicated.

## 2. Code of Conduct

### 2.1 Pledge

In the interest of fostering an open and welcoming environment, we as users, contributors and maintainers of MoodleNet pledge to make participation in our project and our community a harassment-free experience for everyone, regardless of age, body size, disability, ethnicity, sex characteristics, gender identity and expression, level of experience, education, socio-economic status, nationality, personal appearance, race, religion, or sexual identity and orientation.

### 2.2 Encouraged Behaviour

Examples of behaviour that contributes to creating a positive environment include:

- Using welcoming and inclusive language
- Being respectful of differing viewpoints and experiences
- Gracefully accepting constructive criticism
- Focusing on what is best for the community
- Showing empathy towards other community members

### 2.3 Unacceptable Behaviour

Racism, sexism, homophobia, transphobia, harassment, defamation, doxxing, sexual depictions of children, and conduct promoting alt-right and fascist ideologies will not be tolerated.

Other examples of unacceptable behaviour by participants include:

- The use of sexualised language or imagery and unwelcome sexual attention or advances
- Trolling, insulting/derogatory comments, and personal or political attacks
- Public or private harassment
- Publishing others’ private information, such as a physical or electronic address, without explicit permission
- Other conduct which could reasonably be considered inappropriate in a professional setting

### 2.4 Responsibilities of users and contributors

Users and contributors are responsible for watching out for any unacceptable behaviour or content, such as harassment, and bringing it to moderators' attention by using the flagging functionality. If moderators do not respond in a timely or appropriate manner, users are to alert the instance operators, and failing that, Moodle HQ at [moodlenet-moderators@moodle.com](mailto:moodlenet-moderators@moodle.com).

### 2.5 Responsibilities of the project maintainers (such as instance operators, community moderators, and Moodle HQ)

Project maintainers, including instance operators, community moderators, and Moodle HQ, given the relevant access, are responsible for monitoring and acting on flagged content and other user reports, and have the right and responsibility to remove, edit, or reject comments, communities, collections, resources, images, code, wiki edits, issues, and other contributions that are not aligned to this Code of Conduct, or to suspend or ban - temporarily or permanently - any contributor for breaking these terms, or for other behaviours that they deem inappropriate, threatening, offensive, or harmful.

Instance operators should ensure that every community hosted on the instance is properly moderated according to the Code of Conduct.

Project maintainers are responsible for clarifying the standards of acceptable behaviour and are expected to take appropriate and fair corrective action in response to any unacceptable behaviour.

### 2.6 Enforcement

All complaints must be reviewed and investigated by project maintainers, and result in a response that is deemed necessary and appropriate to the circumstances. Project maintainers are obligated to maintain confidentiality with regard to the reporter of an incident. Further details of specific enforcement policies may be posted separately.

Project maintainers who do not follow or enforce the Code of Conduct in good faith may face temporary or permanent repercussions as determined by other maintainers of the project.

### 2.7 Code of Conduct Attribution

This Code of Conduct was adapted from the [Contributor Covenant](https://www.contributor-covenant.org/) ([CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)), [version 1.4](https://www.contributor-covenant.org/version/1/4/code-of-conduct.html).

## 3. Contribution, Use, Modification, and Distribution Licenses

1. Unless otherwise noted, all content or materials that you see, share, contribute, and/or download on this instance is made available under a [Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0) license](https://creativecommons.org/licenses/by-sa/4.0/). This does not necessarily include links to other websites.
2. MoodleNet is powered by free software, meaning that you have the following basic freedoms:
   - The freedom to run the software as you wish, for any purpose.
   - The freedom to study how the software works, and change it so it does your computing as you wish. Access to the source code is a precondition for this.
   - The freedom to redistribute copies so you can help others.
   - The freedom to distribute copies of your modified versions to others. By doing this you can give the whole community a chance to benefit from your changes.
3. Permission is granted to run, study, redistribute, and distribute modified copies of the MoodleNet software according to the terms of the [GNU Affero Public License 3.0](https://www.gnu.org/licenses/agpl-3.0.en.html) (“AGPL”). \
   Note that this is different to the GPL license used for Moodle Core. The AGPL mandates that the source of your MoodleNet instance must be available to be downloaded even if you are providing a service rather than making available a binary. \
   Further information is available at [legal](<https://tldrlegal.com/license/gnu-affero-general-public-license-v3-(agpl-3.0)>).

## 4. Disclaimers

### 4.1. Materials provided 'as is'

The materials on this website have been contributed by other users, and are provided on an 'as is' basis. Neither the site operator, nor Moodle Pty Ltd (“Moodle HQ”), make any warranties, expressed or implied. They hereby disclaim and negate all other warranties including, without limitation, implied warranties or conditions of merchantability, fitness for a particular purpose, or non-infringement of intellectual property or other violation of rights.

### 4.2. Accuracy

Furthermore, neither the site operator, nor Moodle HQ, make any representations concerning the accuracy, likely results, or reliability of use of the materials on this website - which may be incomplete or outdated, or could include technical, typographical, or photographic errors - or relating to such materials or on any sites linked from this site.

Changes may be made to the materials contained on its website at any time without notice. However, neither the site operator nor Moodle HQ make any commitment to update the materials.

### 4.3. Links

Neither the site operator nor Moodle HQ have reviewed all of the sites linked from this website and are not responsible for the contents of any such linked site. The inclusion of any link does not imply endorsement of the site. Use of any such linked website is at the user's own risk.

### 4.4. Limitations

In no event shall the site operator, Moodle HQ, or their suppliers be liable for any damages (including, without limitation, damages for loss of data or profit, or due to business interruption) arising out of the use or inability to use the materials on this website, even if any authorized representative has been notified orally or in writing of the possibility of such damage. Because some jurisdictions do not allow limitations on implied warranties, or limitations of liability for consequential or incidental damages, these limitations may not apply to you.

### 5. Modifications

The site operator may revise these terms of service for its website at any time without notice. By using this website you are agreeing to be bound by the then current version of these terms.

<!-- Copy and paste the converted output. -->

## PRIVACY NOTICE

Last updated 6th of October 2020

This Privacy Notice tells you how we, Moodle Pty Ltd, will collect and use your personal data to provide our MoodleNet service which allows educators, of any kind, to create a private social space online, all optimized for collaborative learning, and connect their installation to a wider network of networks. MoodleNet is the name of one of our pieces of software, which can be installed and hosted by anyone. It includes an option to link to a network of other installations, which we facilitate by running a central API service and search index. We will refer to it as the "mothership".

The MoodleNet software is free and open source, and may be hosted by anyone who wishes to manage an installation. This notice will only tell you how Moodle Pty Ltd uses your MoodleNet personal data, but if the site you are using isn’t hosted by Moodle Pty Ltd, then your data controller will have their own specific Privacy Notice as well, on how your data is used by them.

### Who are we?

Moodle Pty Ltd is a software company which allows educators, of any kind, to create a private space online, filled with tools that easily create courses and activities, all optimized for collaborative learning. MoodleNet is open source, and may be hosted by Moodle Pty Ltd, but also by anyone who wishes to manage an installation.

### What’s covered by this Privacy Notice?

Under the EU’s General Data Protection Regulation (GDPR) personal data is defined as: “any information relating to an identified or identifiable natural person (‘data subject’); an identifiable natural person is one who can be identified, directly or indirectly, in particular by reference to an identifier such as a name, an identification number, location data, an online identifier or to one or more factors specific to the physical, physiological, genetic, mental, economic, cultural or social identity of that natural person”.

All data subjects whose personal data is collected, in line with the requirements of the GDPR.

- where Moodle Pty Ltd is hosting a MoodleNet website on its own behalf, it is the Data Controller for all the data collected.
- On instances _not_ hosted by Moodle Pty Ltd, we provide the "mothership" service that indexes data as well as provides search and discovery across federated instances. In this case, we index public content from other instances to allow this to happen. Our relationship with instance administrators is therefore both as:
  - Joint Data Controller (in respect of certain elements of personal data made public by users which Moodle Pty Ltd makes searchable across the federated instances connected to the "mothership")
  - Data Processor (Instance administrators act as the Data Controller, and can request Moodle Pty Ltd to delete personal data on the data subject from the "mothership")

We are processing personal data for the purposes of identification on a federated social network made up of individually hosted installations of our MoodleNet software. We envisage that this will lead to increased trust and sharing of resources and ideas amongst the educators using federated MoodleNet instances. Users will be able to identify one another, talk about shared interests and goals, and both link to and upload resources that will help their communities. It is not compulsory for 3rd parties to link their instance of MoodleNet to the "mothership". If they do not, none of their users’ personal data will be processed by Moodle Pty Ltd.

We will be collecting users’ personal data, including: username, display name, location, bio, language(s), images, links, resources uploaded, comments, browser version, and IP address. This does not include criminal offence data, but may include special categories such as political beliefs and accessibility requirements. We could also infer ethnicity through avatars, including photographs, that users choose to represent themselves. This would be as a by-product of using the system, through optional rather than mandatory activity (e.g. tagging, photo-upload, discussion replies).

While users may have the ability to hide information about themselves on their profile so that only they can see it, please be aware that this only hides information from other users (as well as from the "mothership"), but not from the operator of the instance.

We want users to be aware that they might, implicitly and explicitly, reveal sensitive information such as their preferences, any disabilities, and ethnicity or location data through what they choose to upload or share on instances of MoodleNet.

### Why does Moodle Pty Ltd need to collect and store personal data?

MoodleNet is a piece of open source software, which can be integrated with an optional service (the MoodleNet "mothership") in order for Moodle Pty Ltd to provide users and instance hosters with a service that indexes data as well as provides search and discovery across federated instances. We need to collect personal data to interact with users and/or to allow us to provide you our service(s). We are committed to ensuring that the information we collect and use is appropriate for this purpose and no more than is necessary and proportionate for those purposes.

Moodle Pty Ltd is a company which values its users’ data protection and privacy rights and we have no interest in collecting data beyond what we need to make our service(s) work for you.

If you are going to be contacted by us for marketing purposes, we will not rely solely on this notice, but will always seek an additional confirmation from you that it’s ok to do that.

**In general we collect personal data relating to you for specific purposes, with the nature of the data collected depending on your interaction with Moodle Pty Ltd. We are committed to transparency in this and have provided a very detailed breakdown of these processes in Annex 1 of this Privacy Notice.**

Our legal basis for the processing of personal data are:

- Article 6.1(a), GDPR, Consent
- Article 6.1(b), GDPR, Contract
- Article 6.1(f), GDPR, Legitimate Interest

The special categories of personal data potentially concerned are:

- biometric data in the form of facial images
- any special categories of special personal data which any user volunteers while using the MoodleNet systems

### Will Moodle Pty Ltd share my personal data with anyone else?

We may pass your personal data on to third-party service providers contracted to Moodle Pty Ltd in the course of dealing with you. We do this because there are some services, which will not work unless we are able to make these transfers. Any third parties that we may share your data with are obliged to keep your details securely, and to use them only to deliver the service they provide on our- and of course- your behalf. When they no longer need your data to fulfil this service, they will dispose of the details in line with Moodle Pty Ltd’s procedures. If we wish to pass your sensitive personal data onto a third party we will only do so once we have obtained your consent, or if it is necessary to comply with a contract, or we are legally required to do so. If you would like an up-to-date register of all our third-party service providers for MoodleNet, please contact [privacy@moodle.com](mailto:privacy@moodle.com) and we will be happy to provide it.

### How will Moodle Pty Ltd use the personal data it collects about me?

Moodle Pty Ltd will process (collect, store and use) the information you provide in a manner compatible with the EU’s General Data Protection Regulation (GDPR). We will endeavour to keep your information accurate and up to date, and not keep it for longer than is necessary. Moodle Pty Ltd maintains a register of its data processes which includes a record of the data retention policy for each type of data collected and is committed to only ever trying to process the minimum amount of data needed. Moodle Pty Ltd is required to retain certain information in accordance with the Law, such as information needed for income tax and audit purposes. How long certain kinds of personal data should be kept may also be governed by specific business-sector requirements and agreed practices. Personal data may be held in addition to these periods depending on individual business needs.

### Can I find out what personal data Moodle Pty Ltd holds about me?

Moodle Pty Ltd at your request, can confirm what information we hold about you and how it is processed. If Moodle Pty Ltd does hold personal data about you, you can request the following information:

- identity and the contact details of the person or organisation that has determined how and why to process your data.
- contact details of our data protection officer in the EU, where applicable.
- the purpose of the processing as well as the legal basis for processing.
- if the processing is based on the legitimate interests of Moodle Pty Ltd or a third party, information about those interests.
- the categories of personal data collected, stored and processed.
- recipient(s) or categories of recipients that the data is/will be disclosed to.
- if we intend to transfer the personal data to a third country or international organisation, information about how we ensure this is done securely.
- the EU has approved sending personal data to some countries because they meet a minimum standard of data protection. In other cases, we will ensure there are specific measures in place to secure your information. These will rely on measures approved by the EU Commission.
- how long the data will be stored.
- details of your rights to correct, erase, restrict or object to such processing.
- information about your right to withdraw consent at any time.
- how to lodge a complaint with the relevant supervisory authority.
- whether the provision of personal data is a statutory or contractual requirement, or a requirement necessary to enter into a contract, as well as whether you are obliged to provide the personal data and the possible consequences of failing to provide such data.
- the source of personal data if it wasn’t collected directly from you.
- any details and information of automated decision making, such as profiling, and any meaningful information about the logic involved, as well as the significance and expected consequences of such processing.

### What forms of ID will I need to provide in order to access this?

Moodle Pty Ltd accepts a request made through a Moodle account while the person making the request is logged in. In certain circumstances Moodle Pty Ltd could ask for additional information and the following forms of ID when information on your personal data is requested:

- A colour copy of a Passport, driving licence or National ID Card

### Agreeing to these terms is your Consent

By consenting to this privacy notice you are giving us permission to process your personal data specifically for the purposes identified. Where consent is required for Moodle Pty Ltd to process both standard and sensitive types of personal data, it must be explicitly given. Where we are asking you for sensitive personal data we will always tell you why and how the information will be used. Agreement with this Privacy Notice and its accompanying terms and conditions (as applicable) (and any Data Processing Agreements, if they apply to you) will be considered to be explicit consent and we will keep a copy of the records of that consent for audit purposes.

You may withdraw consent at any time by:

- **MoodleNet hosted websites:**  
  contacting [privacy@moodle.com](mailto:privacy@moodle.com)
- **self-hosted MoodleNet installations:**  
  contacting the instance's Data Protection Officer or Privacy Officer for your Data Controller (reach out to your instance administrator(s) if you don't know who to contact)

Please identify your role in relation to MoodleNet (if you are an end-user, site admin etc.), and the data you wish to withdraw consent to be processed.

### Disclosure

Moodle Pty Ltd will pass on your personal data to certain third parties. Moodle Pty Ltd is a distributed, global company and it uses cloud services which may be accessed by its employees in any part of the world, including the head office in Australia.

The MoodleNet software platform is open source, so that anyone can install and host a copy of the software, as the data controller. In those circumstances, it will be a matter for the Data Controller to ensure they have also put the necessary safeguards in place for any international transfers outside the EU.

|                                              **Third country (non EU/international organisation)**                                              |                 **Safeguards in place to protect your personal data**                  |
| :---------------------------------------------------------------------------------------------------------------------------------------------: | :------------------------------------------------------------------------------------: |
|                                                 MoodleCloud: Australia, global <br/><br/> <br/>                                                 | Use of the EU's Standard Contract clauses, Privacy Shield, and binding corporate rules |
|                                                Other Moodle sites and services <br/><br/> <br/>                                                 | Use of the EU's Standard Contract clauses, Privacy Shield, and binding corporate rules |
| Other Moodle installations not hosted by Moodle Pty Ltd or additional processing of data from MoodleCloud by the Data Controller<br/><br/><br/> |      To be provided directly to you by the Data Controller for that installation       |

### Retention period

Moodle Pty Ltd will process different forms of personal data for as long as is necessary and proportionate for the purpose for which it has been supplied and will store the personal data for the shortest amount of time possible, taking into account legal and service requirements. For further details on the retention period for any particular type of data, please contact [privacy@moodle.com](mailto:privacy@moodle.com)

### Your rights as a data subject

At any point while we are in possession of or processing your personal data, you, the data subject, have the following rights:

- **right of access** : you have the right to request a copy of the information that we hold about you.
- **right of rectification** : you have a right to correct data that we hold about you that is inaccurate or incomplete.
- **right to be forgotten** : in certain circumstances you can ask for the data we hold about you to be erased from our records.
- **right to restriction of processing** : where certain conditions apply to have a right to restrict the processing.
- **right of portability** : you have the right to have the data we hold about you transferred to another organisation.
- **right to object** : you have the right to object to certain types of processing such as direct marketing.
- **right to object to automated processing, including profiling** : you also have the right to be subject to the legal effects of automated processing or profiling.
- **right to judicial review** : in the event that Moodle Pty Ltd refuses your request under rights of access, we will provide you with a reason as to why. You have the right to complain as outlined in the section named “Complaints” below.

Where Moodle Pty Ltd are your Data Controller, you may make a request directly to the Data Protection Officer using the email address [dpo@moodle.com](mailto:dpo@moodle.com)

Where Moodle Pty Ltd are a Data Processor, and act on behalf of a data controller such as an independently hosted instance of MoodleNet with an API connection to the "mothership", any requests received by Moodle Pty Ltd will be passed on to the Data Controller.

Where Moodle Pty Ltd are not involved with your data, such as where the MoodleNet instance has been self-hosted, you should address your requests to the data controllers of those sites since Moodle Pty Ltd will have no access to your data.

### Complaints

In the event that you wish to make a complaint about how your personal data is being processed by Moodle Pty Ltd (or third parties as described above), or how your complaint has been handled, you have the right to lodge a complaint directly with supervisory authority and also with Moodle Pty Ltd’s Data Protection Officer, Data Compliance Europe.

If you wish to make a complaint about how your personal data has been processed in MoodleCloud, or by a Self-Hosted installation of the Moodle Software you should contact your Moodle Site Admin or the Data Controller for your Moodle installation. (For example, if your university or school hosts their own MoodleNet site, they will be the Data Controller).

The details of Contacts for where Moodle Pty Ltd’s are the Data Controller:

|        **Supervisory authority contact details**        | **Data Protection Officer (DPO) / GDPR Owner contact details** |
| :-----------------------------------------------------: | :------------------------------------------------------------: |
|              Data Protection Commissioner               |                     Data Compliance Europe                     |
|                       Canal House                       |                      Lower Bridge Street                       |
|                      Portarlington                      |                            Dublin 8                            |
|                        Co Laois                         |                                                                |
|                        R32 AP23                         |                                                                |
| [info@dataprotection.ie](mailto:info@dataprotection.ie) |            [dpo@moodle.com](mailto:dpo@moodle.com)             |
|                     +353 57 8684800                     |                         +353 1 6351580                         |

The details of Contacts for where Moodle Pty Ltd is not the Data Controller, in your installation of Moodle are available directly from your Data Controller.

### ANNEX 1

| **CATEGORIES OF PERSONAL DATA**                                                                                                                                                                                                       |                **PURPOSE OF PROCESSING**                 | **THE SOURCE OF PERSONAL DATA** |
| :------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | :------------------------------------------------------: | :-----------------------------: |
| **Display name, avatar, bio, interests, language, <br/>occupation, location, tags** <br/>Could be inferred: workplace, origin, political opinions, <br/>religious beliefs, gender, sexuality, accessibility requirements <br/> <br/>  |                    User registration                     |       Data subject (user)       |
| **Descriptions, language, metadata** <br/>Could be inferred: interests, occupation, location, workplace, location, <br/>origin, political opinions, religious beliefs, gender, <br/>sexuality, accessibility requirements <br/> <br/> | Describing communities, collections, or shared resources |       Data subject (user)       |
| **IP address** <br/>Could be inferred: geolocation of connection to the service <br/> <br/>                                                                                                                                           |             Connecting users to the service              |       Data subject (user)       |
| **Details about browser and device** <br/>Could be inferred: accessibility requirements <br/> <br/>                                                                                                                                   |     Ensuring the service is accessible to all users      |       Data subject (user)       |
| **Search history** <br/>Could be inferred: political beliefs, religious beliefs, <br/>sexuality, accessibility requirements <br/> <br/>                                                                                               |                Improving user experience                 |       Data subject (user)       |
