# Routes Modules

Routing is based on [`react-router`](https://reactrouter.com/web/guides/quick-start).

Routes are defined in `src/routes/` folder.
Each route (with any applicable sub-route) is defined in a single tsx module defining and exporting:

- `RouteProps` to be consumed in `src/containers/App/Router.tsx`
- a simple Stateless `React.FC` component taking `RouteComponentProps` and feeding the wrapping page-controller-component
- a `locationHelper` (`routes/lib/helper#locationHelper`) for an easy and safe url construction for application components

## How to

A router module would typically define an interface with string properties representing `react-router`
