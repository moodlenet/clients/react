# TODO list

## Accomplish stricter MVP guidelines

- [ ] review `src/ui/modules/Burger/index.tsx` and its users. Should be fed with a global UI state `isSidebarOpen`, and not implement local state for it.
- [ ] Get rid of `LocaleContext` (and any other eventual `*Context`) dependencies from UI components and let Ctrls feed UIs properly
- [ ] Get rid of any `*Context` dependencies from Ctrl components and migrate any `Context` handling to `FE`
- [ ] Get rid of remaining hard-coded link urls in UI

## enhance code

- [ ] split `src/apollo/client.tsx` in submodules

## New features

- [ ] Add useful Contexts|AppState (UI state, Flag context, Community context, Collection context, Search Context...)

## Future

- [ ] Global message bus
