# Localisation

[LinguiJS](https://lingui.js.org/) is the localisation library used for i18n.

## Index

- [Set up](#set-up)
- [Usage](#usage)
- [Simple language strings](#simple-language-strings)
- [Language strings as reference](#language-strings-as-reference)
- [Plural language strings](#plural-language-strings)
- [Updating language files](#updating-language-files)

## Set up

- LinguiJS is configured in the `.linguirc` file in the root of the application.

- It comes with a provider component that sets up i18n within the application and makes components
  within the app able to consume the language strings of the user's chosen locale. The provider
  is configured in its Context module (`src/context/global/localizationCtx.tsx`).

## Usage

Wherever you write visible text, i.e. anything user-facing, the copy should be written using the LinguiJS
components. The text used within the LinguiJS components can then be extracted using the CLI operations
provided by the library, which are detailed in the [Scripts](#scripts) section of this document.

Examples of using the LinguiJS library are given below.

### Simple language strings

- First import the [`Trans` component](https://lingui.js.org/ref/react.html#trans):

  ```js
  import { Trans } from '@lingui/macro'
  ```

- _Note:_ the `Trans` component is imported from the `macro` package, not the `react` package!

- Then consume the `Trans` component wherever text is used, like so:

  ```jsx
  <Trans>Sign in using your social media account</Trans>
  ```

### Interpolated language string

It is very common to interpolate values into language strings. This can be done using the `Trans` and `Plural`
components, where the interpolated string names are denoted with curly braces (but still within the actual string)
and the component is given a key/value hash via a `values` prop, where a key of the hash is the name of a string
to be interpolated. For example, from the Login page:

```jsx
<Trans id="You don't need an account to browse {site_name}." values={{ site_name: 'MoodleNet' }} />
```

It is possible then to have `site_name` or any other interpolated string value produced dynamically and inserted
during runtime. If interpolated values also require localisation then you would use a language string hash,
as above in [Language strings as reference](#language-strings-as-reference), making sure to use the `i18nMark`
function to mark them for extraction by the LinguiJS CLI.

### Language strings as reference

When the string is not passed to (as props or directly as children) to the `Trans` component it will not be picked up automatically by the LinguiJS extract script.
In order to "mark" the string as a language string to be included in the compiled language files we must:

- wrap it in a call to `i18nMark` in case of static strings
- produce a `MesageDescriptor` with `t` tag-function in case of interpolated strings.

#### Simple strings

- Import the [`i18nMark` function](https://lingui.js.org/ref/react.html#i18nmark).

  ```js
  import { i18nMark } from '@lingui/react'
  ```

- Define the language string however you like. For example, from the Login page:

  ```js
  const emailRequiredMessage = i18nMark('The email field cannot be empty')
  ```

- _Note:_ the `emailRequiredMessage` string is wrapped in a call to `i18nMark`.

- Then consume the strings.

  ```jsx
  <span class="error">
    <Trans>{emailRequiredMessage}</Trans>
  </span>
  ```

#### Interpolated strings

- Import the [`t` tag-function](https://lingui.js.org/ref/macro.html#t).

  ```js
  import { t } from '@lingui/macro'
  ```

- Define `MessageDescriptor`. For example, for an unavailable username in signup page:

  ```js
  const userNameUnavailable = t`Username {userName} is already taken. Choose another one`
  ```

- _Note:_ the `userNameUnavailable` is not a string it is a [`MessageDescriptor`](https://lingui.js.org/ref/macro.html#js-macros) constructed by the `t` tag-function

- to get an interpolated translation string it has to be consumed by the `i18n._` function exposed by the globally injected Localization `Context`:

  ```jsx
  import { LocaleContext } from 'context/global/localizationCtx';
  ...
  const {i18n} = useContext(LocaleContext)
  ...
  const userUnavailableString = i18n._({ ...userNameUnavailable, values:{ userName: 'nick' } })
  passInterpolatedTranslatedStringToSomeFunction(userUnavailableString)
  ```

### Plural language strings

LinguiJS has a `Plural` component, which is like the `Trans` component but used where the
language contains pluralization.

> `<Plural>` component handles pluralization of words or phrases.
> Selected plural form depends on active language and value props.

The LinguiJS documentation is very comprehensive and should be referred to for usage of the `Plural` component:

[https://lingui.js.org/ref/react.html#plural](https://lingui.js.org/ref/react.html#plural)

## Updating language files

Whenever updates are made to any language within the application you must run the LinguiJS `extract` script.
This will pull out all the language strings and add or update them in the specific locale messages files, which
live in `locales`.

All changes to the language within the application, including changes to the files within `locales`, should
be committed alongside other changes.
